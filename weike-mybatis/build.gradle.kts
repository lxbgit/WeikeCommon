plugins {
    java
    `maven-publish`
}

group = Weike.group
version = Weike.version

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation(Mybatis.dependency)
    implementation(Mybatis.velocity)
    implementation(Dependencies.jedis)
    implementation(Tools.caffeine)
    implementation(Json.Jackson.core)
    implementation(Json.Jackson.annotations)
    implementation(Json.Jackson.databind)
    implementation("com.esotericsoftware:kryo:5.3.0")
}

java {
    withJavadocJar()
    withSourcesJar()
}

configure<JavaPluginExtension> {
    sourceCompatibility = Weike.javaVersion
    targetCompatibility = Weike.javaVersion
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = Weike.group
            artifactId = Weike.Mybatis.artifactId
            version = Weike.version

            from(components["java"])

            pom {
                name.set(Weike.Mybatis.artifactId)
                description.set(" mybatis 扩展")
                url.set("https://gitee.com/lxbgit/WeikeCommon")
                developers {
                    developer {
                        id.set("lxb")
                        name.set("lxb")
                        email.set("lixuebiao1115@126.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitee.com/lxbgit/WeikeCommon.git")
                    developerConnection.set("scm:git:ssh://gitee.com/lxbgit/WeikeCommon.git")
                    url.set("https://gitee.com/lxbgit/WeikeCommon")
                }
            }
        }
    }
    repositories {
        maven {
//            setUrl(uri("http://localhost:8081/repository/maven-releases/"))
//            url = uri("http://localhost:8081/repository/maven-releases/")
            this.isAllowInsecureProtocol = true
            this.setUrl("http://192.168.0.9:8081/repository/maven-releases/")
            credentials {
                username = "admin"
                password = "nexus@JH"
            }
        }
    }
}