package com.lxb.mybatis.velocity.tools;

import cn.hutool.core.util.StrUtil;

import java.util.List;

/**
 * $str.isEmpty($o)
 * $str.isNotEmpty($o)
 */
public class StringTool {

    /**
     * 必须有文本
     * @param object 要判断的对象
     * @return 是否有文本
     */
    public boolean hasText(String object) {
        return StrUtil.isNotBlank(object);
    }

    public boolean isEmpty(String object) {
        return StrUtil.isEmpty(object);//Objects.isNull(object) || object.length() == 0;
    }

    public boolean isNotEmpty(String obj) {
        return StrUtil.isNotEmpty(obj);//!isEmpty(obj);
    }

    public String join(List<CharSequence> list, String de) {
        return String.join(de, list);
    }
}
