package com.lxb.mybatis.velocity.tools;


import cn.hutool.core.util.ObjectUtil;

/**
 * 主要用于velocity 处理
 * object 工具类
 *
 * $obj.isEmpty($o)
 * $obj.isNotEmpty($o)
 */
public class ObjectTool {

    public boolean isEmpty(Object object) {
        return ObjectUtil.isEmpty(object);
    }

    public boolean isNotEmpty(Object obj) {
        return ObjectUtil.isNotEmpty(obj);
    }
}
