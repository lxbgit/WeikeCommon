package com.lxb.mybatis.velocity.tools;


import org.apache.commons.lang3.StringUtils;

public class CollectionTool {

//    public String join(Collection<Object> collection, Character separator) {
//        return CollectionsKt.joinToString(collection, ",", "", "", -1, "...", null);
//    }

    /**
     *
     * @param collection 集合
     * @param separator 分隔符
     * @return [0,1,2,3,4,5] =&gt; "0,1,2,3,4,5"
     */
    public String join(Iterable<Object> collection, CharSequence separator) {
        return StringUtils.join(collection, separator);
//        return CollectionsKt.joinToString(collection, separator, "", "", -1, "...", null);
    }

    /**
     * @param collection 集合
     * @return [0,1,2,3,4,5] =&gt; "0,1,2,3,4,5"
     */
    public String join(Iterable<Object> collection) {
        return StringUtils.join(collection, ",");
//        return CollectionsKt.joinToString(collection, ",", "", "", -1, "...", null);
    }
}
