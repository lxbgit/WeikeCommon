package com.lxb.mybatis.type;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lxb
 * <p>
 * mybatis 处理类型
 * <p>
 * String &lt;-&gt; List&lt;String&gt;
 * <p>
 * 1,2,3,4,5,6 &lt;=&gt; [1, 2, 3, 4, 5, 6]
 */
public class LongSetTypeHandler extends BaseTypeHandler<Set<Long>> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Set<Long> parameter, JdbcType jdbcType) throws SQLException {
        String str = StringUtils.join(parameter, ",");
        ps.setString(i, str);
    }

    @Override
    public Set<Long> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String str = rs.getString(columnName);
        if (StrUtil.isBlank(str)) {
            return Collections.emptySet();
        }
        String[] strArr = str.split(",");
        Set<Long> data = new HashSet<>();
        for (String s : strArr) {
            data.add(Long.parseLong(s));
        }
        return data;
    }

    @Override
    public Set<Long> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String str = rs.getString(columnIndex);
        if (StrUtil.isBlank(str)) {
            return Collections.emptySet();
        }
        String[] strArr = str.split(",");
        Set<Long> data = new HashSet<>();
        for (String s : strArr) {
            data.add(Long.parseLong(s));
        }
        return data;
    }

    @Override
    public Set<Long> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String str = cs.getString(columnIndex);
        if (StrUtil.isBlank(str)) {
            return Collections.emptySet();
        }
        String[] strArr = str.split(",");
        Set<Long> data = new HashSet<>();
        for (String s : strArr) {
            data.add(Long.parseLong(s));
        }
        return data;
    }
}
