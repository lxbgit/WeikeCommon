package com.lxb.mybatis.type;

import cn.hutool.core.collection.ListUtil;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * @author lxb
 * <p>
 * mybatis 处理类型
 * <p>
 * String &lt;-&gt; List&lt;String&gt;
 * <p>
 * a,s,m,d,m,g &lt;=&gt; [a, s, m, d, m, g]
 */
public class StringListTypeHandler extends BaseTypeHandler<List<String>> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType) throws SQLException {
        String str = String.join(",", parameter);
        ps.setString(i, str);
    }

    @Override
    public List<String> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String str = rs.getString(columnName);
        if (str == null) {
            return Collections.emptyList();
        }
        String[] strArr = str.split(",");
        return ListUtil.of(strArr);
    }

    @Override
    public List<String> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String str = rs.getString(columnIndex);
        if (str == null) {
            return Collections.emptyList();
        }
        String[] strArr = str.split(",");
//        return CollectionsKt.listOf(strArr);
        return ListUtil.of(strArr);
    }

    @Override
    public List<String> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String str = cs.getString(columnIndex);
        if (str == null) {
            return Collections.emptyList();
        }
        String[] strArr = str.split(",");
//        return CollectionsKt.listOf(strArr);
        return ListUtil.of(strArr);
    }
}
