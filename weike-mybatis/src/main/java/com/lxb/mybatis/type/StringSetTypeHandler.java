package com.lxb.mybatis.type;

import cn.hutool.core.collection.CollectionUtil;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Set;

/**
 * @author lxb
 * <p>
 * mybatis 处理类型
 * <p>
 * String &lt;-&gt; List&lt;String&gt;
 * <p>
 * a,s,m,d,m,g &lt;=&gt; [a, s, m, d, m, g]
 */
public class StringSetTypeHandler extends BaseTypeHandler<Set<String>> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Set<String> parameter, JdbcType jdbcType) throws SQLException {
        String str = String.join(",", parameter);
        ps.setString(i, str);
    }

    @Override
    public Set<String> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String str = rs.getString(columnName);
        if (str == null) {
            return Collections.emptySet();
        }
        String[] strArr = str.split(",");
        return CollectionUtil.newHashSet(strArr);
//        return SetsKt.hashSetOf(strArr);
    }

    @Override
    public Set<String> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String str = rs.getString(columnIndex);
        if (str == null) {
            return Collections.emptySet();
        }
        String[] strArr = str.split(",");
//        return SetsKt.hashSetOf(strArr);
        return CollectionUtil.newHashSet(strArr);
    }

    @Override
    public Set<String> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String str = cs.getString(columnIndex);
        if (str == null) {
            return Collections.emptySet();
        }
        String[] strArr = str.split(",");
//        return SetsKt.hashSetOf(strArr);
        return CollectionUtil.newHashSet(strArr);
    }
}
