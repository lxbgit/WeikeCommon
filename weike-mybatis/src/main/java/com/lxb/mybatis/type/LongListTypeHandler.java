package com.lxb.mybatis.type;

import cn.hutool.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lxb
 * <p>
 * mybatis 处理类型
 * <p>
 * String &lt;-&gt; List&lt;String&gt;
 * <p>
 * 1,2,3,4,5,6 &lt;=&gt; [1, 2, 3, 4, 5, 6]
 */
public class LongListTypeHandler extends BaseTypeHandler<List<Long>> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<Long> parameter, JdbcType jdbcType) throws SQLException {
        String str = StringUtils.join(parameter, ",");
        ps.setString(i, str);
    }

    @Override
    public List<Long> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String str = rs.getString(columnName);
        if (StrUtil.isBlank(str)) {
            return Collections.emptyList();
        }
        String[] strArr = str.split(",");
        List<Long> list = new ArrayList<>();
        for (String s : strArr) {
            list.add(Long.parseLong(s));
        }
        return list;
    }

    @Override
    public List<Long> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String str = rs.getString(columnIndex);
        if (StrUtil.isBlank(str)) {
            return Collections.emptyList();
        }
        String[] strArr = str.split(",");
        List<Long> list = new ArrayList<>();
        for (String s : strArr) {
            list.add(Long.parseLong(s));
        }
        return list;
    }

    @Override
    public List<Long> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String str = cs.getString(columnIndex);
        if (StrUtil.isBlank(str)) {
            return Collections.emptyList();
        }
        String[] strArr = str.split(",");
        List<Long> list = new ArrayList<>();
        for (String s : strArr) {
            list.add(Long.parseLong(s));
        }
        return list;
    }
}
