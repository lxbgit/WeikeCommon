package com.lxb.mybatis.caches.caffeine;


import com.github.benmanes.caffeine.cache.Caffeine;
import org.apache.ibatis.cache.Cache;

import java.util.concurrent.TimeUnit;


public class CaffeineCache implements Cache {
    private final String id;

    private final com.github.benmanes.caffeine.cache.Cache<Object, Object> cache = Caffeine.newBuilder()
            // 数量上限
            .maximumSize(1024)
            // 过期机制
            .expireAfterWrite(1, TimeUnit.DAYS)
            // 弱引用key
            .weakKeys()
            // 弱引用value
            .weakValues()
            // 剔除监听
            .removalListener((key, value, cause) ->
                    System.out.println("key:" + key + ", value:" + value + ", 删除原因:" + cause.toString()))
            .build();

    public CaffeineCache(String id) {
        this.id = id;
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public void putObject(Object key, Object value) {
        cache.put(key, value);
    }

    @Override
    public Object getObject(Object key) {
        return cache.getIfPresent(key);
    }

    @Override
    public Object removeObject(Object key) {
        Object o = cache.getIfPresent(key);
        cache.invalidate(key);
        return o;
    }

    @Override
    public void clear() {
        cache.invalidateAll();
    }

    @Override
    public int getSize() {
        return (int) cache.estimatedSize();
    }
}
