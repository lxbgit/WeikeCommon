package com.lxb.mybatis.caches.redis;

public interface Serializer {

  /**
   * Serialize method
   *
   * @param object 序列化对象
   * @return serialized bytes
   */
  byte[] serialize(Object object);

  /**
   * Unserialize method
   *
   * @param bytes 反序列化数据
   * @return unserialized object
   */
  Object unserialize(byte[] bytes);

}