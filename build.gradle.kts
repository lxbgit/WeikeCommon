
plugins {
    java
    `java-library`
}

ext["spring-security.version"] = Spring.Security.version

group = Weike.group
version = Weike.version

java.sourceCompatibility = Weike.javaVersion
java.targetCompatibility = Weike.javaVersion

repositories {
    maven(url = "https://repo.spring.io/milestone")
    maven(url = "https://maven.aliyun.com/nexus/content/groups/public/")
    maven(url = "https://maven.aliyun.com/repository/public/")
    maven(url = "https://192.168.0.9:8081/repository/maven-public/")
    mavenCentral()
    mavenLocal()
}

// allprojects是根Project的一个属性
// 块用于添加根工程和所有子工程的配置
allprojects {

    repositories {
        maven(url = "https://repo.spring.io/milestone")
        maven(url = "https://maven.aliyun.com/nexus/content/groups/public/")
        maven(url = "https://maven.aliyun.com/repository/public/")
        maven(url = "https://192.168.0.9:8081/repository/maven-public/")
        mavenCentral()
        mavenLocal()
    }

    tasks.withType<Javadoc>().all {
        enabled = false
    }

}

//subprojects和allprojects一样，
// 也是父Project的一个属性在父项目的build.gradle脚本里，但subprojects()方法用于配置所有的子Project（不包含根Project）
//用于配置子模块信息
subprojects {

    apply {
        plugin("org.gradle.java")
        plugin("org.gradle.java-library")
    }

    configure<JavaPluginExtension> {
        sourceCompatibility = Weike.javaVersion
        targetCompatibility = Weike.javaVersion
    }

    dependencies {
        implementation(Logging.Slf4j.api)
        implementation(Logging.Slf4j.jcl_over_slf4j)
        api(Tools.hutool)

        compileOnly(Tools.lombok)
        annotationProcessor(Tools.lombok)
        testCompileOnly(Tools.lombok)
        testAnnotationProcessor(Tools.lombok)

        testImplementation(Junit.junit5_jupiter_api)
        testImplementation(Junit.junit5_jupiter)
        testImplementation(Junit.junit5_jupiter_engine)
    }
}
