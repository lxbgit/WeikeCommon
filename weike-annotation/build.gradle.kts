
plugins {
    java
    `java-library`
}

group = Weike.group
version = Weike.version

java.sourceCompatibility = Weike.javaVersion
java.targetCompatibility = Weike.javaVersion

dependencies {
    implementation("org.springframework:spring-core:5.3.22")
    implementation("org.springframework:spring-web:5.3.22")
}

//configure<JavaPluginConvention> {
//    sourceCompatibility = Weike.javaVersion
//    targetCompatibility = Weike.javaVersion
//}
