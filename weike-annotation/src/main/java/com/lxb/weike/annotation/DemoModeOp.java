package com.lxb.weike.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 演示模式下是否可以操作，默认不可操作
 *
 * 通过拦截器拦截判断是否可在演示模式下操作
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DemoModeOp {

    @AliasFor("isAllow")
    boolean value() default false;

    @AliasFor("value")
    boolean isAllow() default false;
}
