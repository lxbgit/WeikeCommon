# 简介

> common 基础公共包


```
WeikeCommon (pom)
    ├──── doc // 文档
    ├──── buildSrc // 构建
    ├──── weike-commons // 公共工具类
    ├──── weike-commons-ktx //kotlin扩展
    ├──── weike-common-codegen //代码生成
    ├──── weike-annotation // 注解
    ├──── weike-annotation-processor // 注解
    ├──── weike-model // 基础model
    ├──── weike-mybatis // mybait 扩展
```

# ChangeLog

## update at 2022-05-29

- 重构文档
