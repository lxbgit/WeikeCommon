import org.gradle.api.JavaVersion

object Weike {

    val javaVersion = JavaVersion.VERSION_1_8
//    const val jvmTarget = "1.8"

    const val version = "2.9.2"
    const val group = "com.lxb"

    const val annotation = ":weike-annotation"
//    const val annotation_processor = ":weike-annotation-processor"
//    const val model = ":weike-model"
    const val commons = ":weike-commons"
    const val mybatis = ":weike-mybatis"


    object Common {
        const val artifactId = "common-tools"
        const val ktxArtifactId = "common-tools-ktx"
        const val springArtifactId = "common-tools-spring"
    }

    object Mybatis {
        const val artifactId = "mybatis-ext"
    }
}

object Spring {

    object Framework {
        // https://mvnrepository.com/artifact/org.springframework/spring-context
        const val version = "6.0.0-RC1"
    }

    object Security {
        // https://mvnrepository.com/artifact/org.springframework.security/spring-security-core
        const val version = "6.0.0-RC2"
    }
}

object Tools {
    // commons-lang3 的替代者
    // https://mvnrepository.com/artifact/cn.hutool/hutool-all
    const val hutool = "cn.hutool:hutool-all:5.8.8"

    // https://mvnrepository.com/artifact/com.github.liaochong/myexcel
    const val myexcel = "com.github.liaochong:myexcel:4.2.1"

    // https://mvnrepository.com/artifact/org.projectlombok/lombok
    const val lombok = "org.projectlombok:lombok:1.18.24"

    // https://mvnrepository.com/artifact/com.drewnoakes/metadata-extractor
    const val metadata_extractor = "com.drewnoakes:metadata-extractor:2.18.0"

    // https://mvnrepository.com/artifact/org.jsoup/jsoup
    const val jsonp = "org.jsoup:jsoup:1.15.1"

    // https://mvnrepository.com/artifact/com.github.ben-manes.caffeine/caffeine
    const val caffeine = "com.github.ben-manes.caffeine:caffeine:2.9.3"

}

object Junit {
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
    private const val junit5_version = "5.8.2"
    const val junit5_jupiter_api = "org.junit.jupiter:junit-jupiter-api:$junit5_version"
    const val junit5_jupiter = "org.junit.jupiter:junit-jupiter:$junit5_version"
    const val junit5_jupiter_engine = "org.junit.jupiter:junit-jupiter-engine:$junit5_version"
}

object Logging {

    object Slf4j {
        private const val version = "1.7.36"

        // https://mvnrepository.com/artifact/org.slf4j/slf4j-api
        const val api = "org.slf4j:slf4j-api:$version"
        const val jcl_over_slf4j = "org.slf4j:jcl-over-slf4j:$version"
    }

    object Logback {
//     https://mvnrepository.com/artifact/ch.qos.logback/logback-core
        private const val v = "1.2.11"
        const val classic = "ch.qos.logback:logback-classic:${v}"
        const val core = "ch.qos.logback:logback-core:${v}"
    }
}

object Json {

    object Jackson {
        private const val version = "2.13.3"

        const val core = "com.fasterxml.jackson.core:jackson-core:$version"
        const val annotations = "com.fasterxml.jackson.core:jackson-annotations:$version"
        const val databind = "com.fasterxml.jackson.core:jackson-databind:$version"
    }

    // https://mvnrepository.com/artifact/com.google.code.gson/gson
    const val gson = "com.google.code.gson:gson:2.9.0"

//    /**
//     * JSON Web Token
//     */
//    object WebToken {
//        private const val version = "0.11.5"
//
//        // https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt-api
//        const val jjwt_api = "io.jsonwebtoken:jjwt-api:$version"
//        const val jjwt_impl = "io.jsonwebtoken:jjwt-impl:$version"
//        const val jackson = "io.jsonwebtoken:jjwt-jackson:$version"
//    }
}

object Mybatis {
    // https://mvnrepository.com/artifact/org.mybatis/mybatis
    const val dependency = "org.mybatis:mybatis:3.5.10"

    // https://mvnrepository.com/artifact/org.mybatis.scripting/mybatis-velocity
    const val velocity = "org.mybatis.scripting:mybatis-velocity:2.1.1"

    // https://mvnrepository.com/artifact/org.mybatis/mybatis-spring
    const val spring = "org.mybatis:mybatis-spring:2.0.7"
}

object Jakarta {
    // https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api
    const val validation_api = "jakarta.validation:jakarta.validation-api:3.0.2" //3.0.0

    // https://mvnrepository.com/artifact/jakarta.servlet/jakarta.servlet-api
    const val servlet_api = "jakarta.servlet:jakarta.servlet-api:4.0.4" //5.0.0

    // https://mvnrepository.com/artifact/com.sun.mail/jakarta.mail
    const val mail = "com.sun.mail:jakarta.mail:1.6.7" // 2.0.1
}

object Dependencies {
    // https://mvnrepository.com/artifact/org.aspectj/aspectjweaver
    const val aspectjweaver = "org.aspectj:aspectjweaver:1.9.6"

    // https://mvnrepository.com/artifact/redis.clients/jedis
    const val jedis = "redis.clients:jedis:3.6.0"
}

// https://mvnrepository.com/artifact/com.google.zxing/core
object Zxing {
    private const val version = "3.5.0"
    const val core = "com.google.zxing:core:$version"
    const val javase = "com.google.zxing:javase:$version"
}

object Kotlin {
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib-common
    const val version = "1.7.20"
    const val plugin = "kotlin"
    const val jvm = "jvm"
}
