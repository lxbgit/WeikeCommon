package com.weike.gradle.dsl

import org.gradle.api.artifacts.dsl.DependencyHandler


/**
 * @author tiger
 * -- create at 2019/11/22 16:01
 */
fun DependencyHandler.spring_boot(module: String, version: String? = null): Any =
        "org.springframework.boot:spring-boot-starter-$module${version?.let { ":$version" } ?: ""}"
