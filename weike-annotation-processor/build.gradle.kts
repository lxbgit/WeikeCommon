
plugins {
    java
}

group = Weike.group
version = Weike.version

java.sourceCompatibility = Weike.javaVersion
java.targetCompatibility = Weike.javaVersion

dependencies {
    implementation("org.springframework:spring-web:5.3.22")
    implementation("org.springframework:spring-webmvc:5.3.22")
    implementation("org.springframework.security:spring-security-core:5.7.2")

    implementation(project(Weike.annotation))
    implementation(Json.Jackson.core)
    implementation(Json.Jackson.annotations)
    implementation(Json.Jackson.databind)
    implementation(Jakarta.servlet_api)
}

//configure<JavaPluginConvention> {
//    sourceCompatibility = Weike.javaVersion
//    targetCompatibility = Weike.javaVersion
//}
