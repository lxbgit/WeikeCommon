package com.lxb.weike.annotation;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SupportedOptions({"CLASSNAME"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({
        "com.lxb.weike.annotation.PermissionDef"
})
public class AuthorityProcessor extends AbstractProcessor {

    private Messager msg;

    private Filer filer;  //用于文件处理

    private ObjectMapper objectMapper;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations == null || annotations.isEmpty()) {
            return false;
        }

        for (TypeElement te : annotations) {
            msg.printMessage(Diagnostic.Kind.NOTE, "注解:" + te.getSimpleName());
//            msg.printMessage(Diagnostic.Kind.NOTE, te.getQualifiedName());

            Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(te);
            List<Map<String, Object>> authList = new ArrayList<>();

            for (Element e : elements) {
                if (e instanceof TypeElement) {
                    // 如果是class或者interface, 不做处理
                    continue;
                }

                if (e instanceof ExecutableElement) {
//                    // 如果是方法，那么就处理
//                    Map<String, Object> result = new HashMap<>();
//
//                    ExecutableElement ee = (ExecutableElement) e;
//                    Element parentEle = ee.getEnclosingElement();
//
////                    PermissionDef parentRd = parentEle.getAnnotation(PermissionDef.class);
//                    PermissionDef rd = ee.getAnnotation(PermissionDef.class);
//                    String name = "";
//                    String desc = "";
//                    if (parentRd != null) {
//                        name = name + parentRd.identification() + "_";
//                        desc = parentRd.description();
//                    }
//                    if (rd != null) {
//                        name = name + rd.text();
//                        desc = rd.id();
//                    }
//
//                    result.put("authorityName", name);
//                    result.put("remark", desc);
//
//                    PreAuthorize gm = ee.getAnnotation(PreAuthorize.class);
//                    if (gm != null) {
//                        String authValue = gm.value();
//                        Set<String> set = parseAuthorities(authValue);
//                        result.put("authorityKey", set);
//                    }
//                    authList.add(result);
                }
            }
            genToFile(authList);
        }
        return false;
    }

    private void genToFile(List<Map<String, Object>> mappings) {
        FileOutputStream fos = null;
//        OutputStreamWriter writer = null;
        try {
            FileObject resource = filer.createResource(StandardLocation.CLASS_OUTPUT, "", "customAuthorities.json");
            String resourcePath = resource.toUri().getPath();
            // 由于我们想要把json文件生成在app/src/main/assets/目录下,所以这里可以对字符串做一个截取，
            // 以此便能准确获取项目在每个电脑上的 /app/src/main/assets/的路径
//            String appPath = resourcePath.substring(0, resourcePath.indexOf("app") + 4);
//            String assetsPath = appPath + "src/main/resources";

            File file = new File(resourcePath);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            // 利用fastjson把收集到的所有的页面信息 转换成JSON格式的。并输出到文件中
//            String content = JSON.toJSONString(mappings);
            fos = new FileOutputStream(file);
//            writer = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
            objectMapper.writeValue(fos, mappings);
//            writer.write(content);
//            writer.flush();
            msg.printMessage(Diagnostic.Kind.NOTE, "path = " + resourcePath);
            msg.printMessage(Diagnostic.Kind.NOTE, mappings.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Set<String> parseAuthorities(String auth) {
        Pattern p = Pattern.compile("\\([a-z_\\-':,\\s]+\\)");
        Matcher m = p.matcher(auth);
        Set<String> authList = new HashSet<>();
        while (m.find()) {
            String g = m.group().replace("'", "");
            g = g.substring(1, g.length() - 1);
            String[] arr = g.split(",");
            for (String a : arr) {
                authList.add(a.trim());
            }
        }
        return authList;
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        msg = processingEnv.getMessager();
        //文件处理工具
        this.filer = processingEnv.getFiler();
        this.objectMapper = new ObjectMapper();
    }
}
