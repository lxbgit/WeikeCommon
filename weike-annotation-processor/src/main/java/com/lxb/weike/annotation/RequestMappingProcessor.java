package com.lxb.weike.annotation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

@SupportedOptions({"CLASSNAME"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({
        "org.springframework.web.bind.annotation.RequestMapping",
        "org.springframework.web.bind.annotation.GetMapping",
        "org.springframework.web.bind.annotation.PostMapping",
        "org.springframework.web.bind.annotation.PutMapping",
        "org.springframework.web.bind.annotation.PatchMapping",
        "org.springframework.web.bind.annotation.DeleteMapping"
})
public class RequestMappingProcessor extends AbstractProcessor {

    private Messager msg;

    private Filer filer;  //用于文件处理

    private ObjectMapper objectMapper;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations == null || annotations.isEmpty()) {
            return false;
        }

        for (TypeElement te : annotations) {
            msg.printMessage(Diagnostic.Kind.NOTE, "注解为：" + te.getQualifiedName());
//            msg.printMessage(Diagnostic.Kind.NOTE, te.getQualifiedName());
//            msg.printMessage(Diagnostic.Kind.NOTE,  te.getSimpleName());
//            msg.printMessage(Diagnostic.Kind.NOTE, roundEnv.toString());

            Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(te);

            List<Map<String, Object>> mappings = new ArrayList<>();
            for (Element e : elements) {
                if (e instanceof TypeElement) {
                    // 如果是class或者interface, 不做处理
                    continue;
                }

                if (e instanceof ExecutableElement) {
                    Map<String, Object> result = new HashMap<>();
                    // 如果是方法，那么就处理
                    ExecutableElement ee = (ExecutableElement) e;
                    Element parentEle = ee.getEnclosingElement();

                    String[] parentPathArr = new String[]{};
                    RequestMapping parentMapping = parentEle.getAnnotation(RequestMapping.class);
                    if (parentMapping != null) {
                        parentPathArr = parentMapping.value();
                    }
                    RequestDescription parentDesc = parentEle.getAnnotation(RequestDescription.class);
                    RequestDescription childDesc = ee.getAnnotation(RequestDescription.class);
                    String name = "";
                    String desc = "";
                    if (parentDesc != null) {
                        name = parentDesc.value() + "_";
                        desc = parentDesc.description();
                    }
                    if (childDesc != null) {
                        name = name + childDesc.value();
                        desc = childDesc.description();
                    }
                    result.put("resourceName", name);
                    result.put("resourceDesc", desc);

                    GetMapping gm = ee.getAnnotation(GetMapping.class);
                    if (gm != null) {
                        String[] childPathArr = gm.value();
//                        msg.printMessage(Diagnostic.Kind.NOTE, name + parsePath(parentPathArr, childPathArr));
                        result.put("resourceUrl", parsePath(parentPathArr, childPathArr));
                        mappings.add(result);
                        continue;
                    }
                    PostMapping pm = ee.getAnnotation(PostMapping.class);
                    if (pm != null) {
                        String[] childPathArr = pm.value();
//                        msg.printMessage(Diagnostic.Kind.NOTE, name + parsePath(parentPathArr, childPathArr));
                        result.put("resourceUrl", parsePath(parentPathArr, childPathArr));
                        mappings.add(result);
                        continue;
                    }

                    DeleteMapping dm = ee.getAnnotation(DeleteMapping.class);
                    if (dm != null) {
                        String[] childPathArr = dm.value();
//                        msg.printMessage(Diagnostic.Kind.NOTE, name + parsePath(parentPathArr, childPathArr));
                        result.put("resourceUrl", parsePath(parentPathArr, childPathArr));
                        mappings.add(result);
                        continue;
                    }

                    RequestMapping rm = ee.getAnnotation(RequestMapping.class);
                    if (rm != null) {
                        String[] childPathArr = rm.value();
//                        msg.printMessage(Diagnostic.Kind.NOTE, name + parsePath(parentPathArr, childPathArr));
                        result.put("resourceUrl", parsePath(parentPathArr, childPathArr));
                        mappings.add(result);
                    }
                }
            }

            genToFile(te.getSimpleName().toString(), mappings);
        }
        return false;
    }

    private void genToFile(String fileName, List<Map<String, Object>> mappings) {
        FileOutputStream fos = null;
//        OutputStreamWriter writer = null;
        try {
            FileObject resource = filer.createResource(StandardLocation.CLASS_OUTPUT, "", fileName + ".json");
            String resourcePath = resource.toUri().getPath();
            // 由于我们想要把json文件生成在app/src/main/assets/目录下,所以这里可以对字符串做一个截取，
            // 以此便能准确获取项目在每个电脑上的 /app/src/main/assets/的路径
//            String appPath = resourcePath.substring(0, resourcePath.indexOf("app") + 4);
//            String assetsPath = appPath + "src/main/resources";

            File file = new File(resourcePath);
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            // 利用fastjson把收集到的所有的页面信息 转换成JSON格式的。并输出到文件中
//            String content = JSON.toJSONString(mappings);
            fos = new FileOutputStream(file);
            objectMapper.writeValue(fos, mappings);
//            writer = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
//            writer.write(content);
//            writer.flush();
            msg.printMessage(Diagnostic.Kind.NOTE, "path = " + resourcePath);
            msg.printMessage(Diagnostic.Kind.NOTE, mappings.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<String> parsePath(String[] parentPaths, String[] childPaths) {
        if (parentPaths.length == 0) {
            return Arrays.asList(childPaths);
        }
        List<String> paths = new ArrayList<>();

        for (String pp : parentPaths) {
            for (String cp : childPaths) {
                paths.add(pp + cp);
            }
        }

        return paths;
    }


    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        msg = processingEnv.getMessager();
        //文件处理工具
        this.filer = processingEnv.getFiler();
        this.objectMapper = new ObjectMapper();
    }
}
