package com.weike.commons.util;

import cn.hutool.core.util.StrUtil;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrUtilTest {

    @Test
    public void testParseEl() {
//        ExpressionParser parser = new SpelExpressionParser();
//        Expression expression = parser.parseExpression("isFullyAuthenticated() and hasAnyAuthority('system:authority:list')", new TemplateParserContext());
//        System.out.println(expression);

        String s = "isFullyAuthenticated() and hasAnyAuthority('system:authority:list', 'ssss') or hasRole('admin', 'superadmin')";

//        int a = s.indexOf("hasAnyAuthority");

//        Regex
//        s.matches("^([a-z0-9]+)$");
        Pattern p = Pattern.compile("\\([a-z_\\-':,\\s]+\\)");
        Matcher m = p.matcher(s);
        while (m.find()) {
            System.out.println("======");
            System.out.println(m.group());
        }

//        System.out.println(s.substring(a));
    }

    @Test
    public void test() {
//        String s = StrUtils.underlineToHump2("base_info_propaganda");
//        System.out.println(s);

        String s = StrUtil.toCamelCase("base_info_propaganda");
        System.out.println(s);
    }
}
