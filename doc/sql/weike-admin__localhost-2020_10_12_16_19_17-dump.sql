-- MySQL dump 10.13  Distrib 8.0.20, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: weike-admin
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pub_authority`
--

DROP TABLE IF EXISTS `pub_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_authority` (
  `authority_id` bigint NOT NULL,
  `authority_name` varchar(64) NOT NULL,
  `authority_key` varchar(64) DEFAULT NULL COMMENT '权限key值（x:x:x:x）',
  `is_enabled` bit(1) NOT NULL DEFAULT b'1',
  `remark` varchar(128) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_authority`
--

LOCK TABLES `pub_authority` WRITE;
/*!40000 ALTER TABLE `pub_authority` DISABLE KEYS */;
INSERT INTO `pub_authority` (`authority_id`, `authority_name`, `authority_key`, `is_enabled`, `remark`, `create_time`, `update_time`) VALUES (1111111111111,'[超级权限]','super-admin',_binary '','Super Admin','2020-07-09 21:21:02','2020-07-09 21:21:08'),(1377925254409,'[系统管理]_[机构管理]','SYSTEM:ORG',_binary '','机构管理','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1377925320407,'[系统管理]_[用户管理]','system:user',_binary '','用户管理','2019-08-30 12:02:47','2020-07-28 15:56:03'),(1377925329991,'[系统管理]_[角色管理]','system:role',_binary '','角色管理','2019-08-30 12:02:47','2020-07-28 15:55:47'),(1377925337277,'[系统管理]_[权限管理]','system:authority',_binary '','[系统管理]_[权限管理]','2019-08-30 12:02:47','2020-07-10 17:44:00'),(1377925351603,'[系统管理]_[菜单管理]','system:menu',_binary '','菜单管理','2019-08-30 12:02:47','2020-07-28 15:55:21'),(1385021957879,'[系统管理]_[参数管理]','system:param',_binary '','参数管理','2019-08-30 12:02:47','2020-07-28 15:54:57'),(1472450956478,'测试用例1','TEST1',_binary '','测试用例1 仅用于测试','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1472450971285,'测试用例2','TEST2',_binary '','测试用例2 仅用于测试','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1472450976416,'测试用例3','TEST3',_binary '','测试123456','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1475889178590,'[系统默认]','SYSTEM:DEFAULT',_binary '','默认的系统权限','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1475891882706,'[系统管理]_[数据字典]','system:dictionary',_binary '','系统管理下的数据字典管理','2019-08-30 12:02:47','2020-07-10 17:43:13'),(1475894194473,'[系统管理]','SYSTEM',_binary '','系统管理的权限','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1550653995378,'[邮箱管理]_发件箱','MAIL:SEND',_binary '','邮件管理 发件箱','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1550730903140,'[邮箱管理]','MAIL',_binary '','邮箱管理','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1550730926998,'[邮箱管理]_收件箱','MAIL:RECEIVE',_binary '','[邮箱管理]_收件箱','2019-08-30 12:02:47','2019-08-30 12:02:48'),(1551075655698,'[邮箱管理]_[草稿箱]','mail:draft',_binary '','邮箱管理_草稿箱','2019-08-30 12:02:47','2020-07-28 15:56:40'),(562584829640572928,'日志管理','system:log',_binary '','日志管理权限','2019-08-30 12:02:47','2020-07-10 17:43:34'),(572020202649157632,'[内容管理]_[新闻模块]','CONTENT:NEWS',_binary '','内容管理 新闻模块(菜单)','2019-08-30 12:02:47','2019-08-30 12:02:48'),(614401253123817472,'[系统管理]_[系统工具]_[代码生成]','SYSTEM:TOOL:CODEGEN',_binary '','代码生成','2019-08-30 12:02:47','2019-08-30 12:02:48'),(614852257971699712,'演示案例','DEMOS',_binary '','演示案例','2019-08-30 12:02:47','2019-08-30 12:02:48'),(615844508373876736,'[系统管理]_[文件管理]','system:file',_binary '','[系统管理]_[文件管理]','2019-08-30 12:02:47','2020-07-10 17:44:20'),(615958614380642304,'[系统管理]_[系统工具]','SYSTEM:TOOL',_binary '','系统工具','2019-08-30 12:02:47','2019-08-30 12:02:48'),(730184435856572416,'[系统管理]_[机构管理]_[机构删除]','system:org:delete',_binary '','删除机构','2020-07-07 22:12:15','2020-07-07 22:12:15'),(731203661669072896,'[系统管理]_[菜单管理]_[菜单删除]','system:menu:delete',_binary '','[系统管理]_[菜单管理]_[菜单删除]','2020-07-10 17:42:18','2020-07-10 17:42:18'),(731235110652739584,'[系统管理]_[菜单管理]_[菜单新增]','system:menu:create',_binary '','[系统管理]_[菜单管理]_[菜单新增]','2020-07-10 19:47:16','2020-07-10 19:47:16'),(731235339728846848,'[系统管理]_[菜单管理]_[菜单更新]','system:menu:update',_binary '','[系统管理]_[菜单管理]_[菜单更新]','2020-07-10 19:48:10','2020-07-10 19:48:10'),(731460575711723520,'[系统管理]_[菜单管理]_[注册权限]','system:menu:authority:register',_binary '','给菜单注册权限','2020-07-11 10:43:11','2020-07-11 10:43:11'),(731466798716682240,'[系统管理]_[菜单管理]_[删除权限]','system:menu:authority:delete',_binary '','删除菜单的权限','2020-07-11 11:07:54','2020-07-11 11:07:54'),(731467009044250624,'[系统管理]_[菜单管理]_[权限添加]','system:menu:authority:create',_binary '','为菜单添加权限','2020-07-11 11:08:44','2020-07-11 11:08:44'),(731474111343099904,'[系统管理]_[资源管理]_[注册权限]','system:resource:authority:update',_binary '','给资源注册权限','2020-07-11 11:36:58','2020-07-11 11:36:58');
/*!40000 ALTER TABLE `pub_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_authority_resource`
--

DROP TABLE IF EXISTS `pub_authority_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_authority_resource` (
  `authority_id` bigint NOT NULL,
  `resource_id` bigint NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`authority_id`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_authority_resource`
--

LOCK TABLES `pub_authority_resource` WRITE;
/*!40000 ALTER TABLE `pub_authority_resource` DISABLE KEYS */;
INSERT INTO `pub_authority_resource` (`authority_id`, `resource_id`, `create_time`, `update_time`) VALUES (1377925254409,560131716690214912,'2019-08-30 12:03:06','2019-08-31 03:03:07'),(1377925254409,560136929425752064,'2019-08-30 12:03:06','2019-08-31 04:03:07'),(1377925254409,560144671087525888,'2019-08-30 12:03:06','2019-08-31 05:03:07'),(1377925320407,560145104581427200,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(1377925320407,560145187582509056,'2019-08-30 12:03:06','2019-08-31 03:03:07'),(1377925320407,560145278183669760,'2019-08-30 12:03:06','2019-08-31 04:03:07'),(1377925320407,560145529888047104,'2019-08-30 12:03:06','2019-08-31 05:03:07'),(1377925320407,566670610814468096,'2019-08-30 12:03:06','2019-08-31 06:03:07'),(1377925329991,560146477360349184,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(1377925329991,560146544100114432,'2019-08-30 12:03:06','2019-08-31 03:03:07'),(1377925329991,560146596721852416,'2019-08-30 12:03:06','2019-08-31 04:03:07'),(1377925329991,560146679995564032,'2019-08-30 12:03:06','2019-08-31 05:03:07'),(1377925329991,560146785939488768,'2019-08-30 12:03:06','2019-08-31 06:03:07'),(1377925337277,560147341567328256,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(1377925337277,560147425969307648,'2019-08-30 12:03:06','2019-08-31 01:04:07'),(1377925337277,560147501450002432,'2019-08-30 12:03:06','2019-08-31 01:05:07'),(1377925337277,560147545133678592,'2019-08-30 12:03:06','2019-08-31 01:06:07'),(1377925337277,560147677975674880,'2019-08-30 12:03:06','2019-08-31 01:07:07'),(1377925351603,560157692971188224,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(1377925351603,560387355349876736,'2019-08-30 12:03:06','2019-08-31 03:03:07'),(1377925351603,560387452984885248,'2019-08-30 12:03:06','2019-08-31 04:03:07'),(1385021957879,560158370531639296,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(1385021957879,560158553654951936,'2019-08-30 12:03:06','2019-08-31 03:03:07'),(1385021957879,560387536724164608,'2019-08-30 12:03:06','2019-08-31 04:03:07'),(1385021957879,560387595398283264,'2019-08-30 12:03:06','2019-08-31 05:03:07'),(1475889178590,560512300117655552,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475889178590,560512489406595072,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475889178590,560512663063363584,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475891882706,560387114890428416,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475891882706,560387658128293888,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475891882706,560387714940141568,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475891882706,560387771554856960,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475891882706,561571852711886848,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560131716690214912,'2019-08-30 12:03:06','2019-08-31 01:13:07'),(1475894194473,560136929425752064,'2019-08-30 12:03:06','2019-08-31 01:03:17'),(1475894194473,560144671087525888,'2019-08-30 12:03:06','2019-08-31 01:03:27'),(1475894194473,560145104581427200,'2019-08-30 12:03:06','2019-08-31 01:03:37'),(1475894194473,560145187582509056,'2019-08-30 12:03:06','2019-08-31 01:03:47'),(1475894194473,560145278183669760,'2019-08-30 12:03:06','2019-08-31 01:03:57'),(1475894194473,560145529888047104,'2019-08-30 12:03:06','2019-08-31 01:23:07'),(1475894194473,560146477360349184,'2019-08-30 12:03:06','2019-08-31 01:33:07'),(1475894194473,560146544100114432,'2019-08-30 12:03:06','2019-08-31 01:43:07'),(1475894194473,560146596721852416,'2019-08-30 12:03:06','2019-08-31 01:53:07'),(1475894194473,560146679995564032,'2019-08-30 12:03:06','2019-08-31 11:03:07'),(1475894194473,560146785939488768,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(1475894194473,560147341567328256,'2019-08-30 12:03:06','2019-08-31 03:03:07'),(1475894194473,560147425969307648,'2019-08-30 12:03:06','2019-08-31 04:03:07'),(1475894194473,560147501450002432,'2019-08-30 12:03:06','2019-08-31 05:03:07'),(1475894194473,560147545133678592,'2019-08-30 12:03:06','2019-08-31 06:03:07'),(1475894194473,560147677975674880,'2019-08-30 12:03:06','2019-08-31 07:03:07'),(1475894194473,560157692971188224,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560158370531639296,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560158553654951936,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560387355349876736,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560387452984885248,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560387536724164608,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(1475894194473,560387595398283264,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(562584829640572928,562584572999499776,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(572020202649157632,572021254354108416,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(572020202649157632,615961211892137984,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(614401253123817472,615542922468655104,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(614401253123817472,615543151809003520,'2019-08-30 12:03:06','2019-08-30 12:03:07'),(615844508373876736,615844208279814144,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(615958614380642304,615542922468655104,'2019-08-30 12:03:06','2019-08-31 02:03:07'),(615958614380642304,615543151809003520,'2019-08-30 12:03:06','2019-08-31 03:03:07');
/*!40000 ALTER TABLE `pub_authority_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_codegen_table`
--

DROP TABLE IF EXISTS `pub_codegen_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_codegen_table` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `project_id` int NOT NULL COMMENT '项目id',
  `table_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '表名',
  `table_prefix` varchar(48) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '表前缀',
  `class_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类名',
  `pkg_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类所在包下',
  `module_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '该实体所在模块',
  `table_comment` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '表备注',
  `class_comment` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类注释，不填则为tb_comment',
  `table_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '表类型，视图或者表',
  `engine` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据库引擎',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pub_codegen_tb_table_name_uindex` (`table_name`),
  KEY `pub_codegen_table_project_id_index` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='代码生成的表，由用户在管理系统生成';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_codegen_table`
--

LOCK TABLES `pub_codegen_table` WRITE;
/*!40000 ALTER TABLE `pub_codegen_table` DISABLE KEYS */;
INSERT INTO `pub_codegen_table` (`id`, `project_id`, `table_name`, `table_prefix`, `class_name`, `pkg_name`, `module_name`, `table_comment`, `class_comment`, `table_type`, `engine`, `update_time`, `create_time`) VALUES (1,1,'t_art','t_','Art',NULL,NULL,'测试表生成','类描述',NULL,NULL,'2020-05-24 18:20:39','2020-04-08 09:51:32'),(2,1,'m_demand','m_','Demand',NULL,NULL,'需求提报表','需求提报表',NULL,NULL,'2020-05-24 18:20:39','2020-04-08 09:51:32'),(3,1,'m_data_table','m_','DataTable',NULL,NULL,'数据表','数据表',NULL,NULL,'2020-05-24 18:20:39','2020-04-08 09:51:32'),(4,1,'m_process_log','m_','ProcessLog',NULL,NULL,'流程表（process_log）','流程表（process_log）',NULL,NULL,'2020-05-24 18:20:39','2020-04-08 09:51:32'),(5,1,'m_question_feedback','m_','QuestionFeedback',NULL,NULL,'问题反馈表','问题反馈表',NULL,NULL,'2020-05-24 18:20:39','2020-04-08 09:51:32'),(6,0,'t_you','t_','You',NULL,NULL,'测试代码','测试代码',NULL,NULL,'2020-06-24 11:25:21','2020-06-24 11:25:21'),(7,0,'t_phone','t_','Phone',NULL,NULL,'手机信息','手机信息',NULL,NULL,'2020-06-24 11:26:34','2020-06-24 11:26:34'),(8,6,'t_icon','t_','Icon',NULL,NULL,'图标数据表','图标数据表',NULL,NULL,'2020-06-24 20:52:21','2020-06-24 20:52:21'),(9,6,'t_me','t_','Me',NULL,NULL,'测试表二','测试表二',NULL,NULL,'2020-06-24 20:53:11','2020-06-24 20:53:11');
/*!40000 ALTER TABLE `pub_codegen_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_codegen_table_column`
--

DROP TABLE IF EXISTS `pub_codegen_table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_codegen_table_column` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_id` int DEFAULT NULL COMMENT '表id',
  `table_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对应表名',
  `column_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '数据库表对应列表',
  `member_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对应生成的类中的变量名称',
  `data_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '列名对应类型',
  `member_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '变量类型',
  `query_type` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '查询类型',
  `column_key` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '如果是主键,为PRI',
  `column_comment` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '列备注',
  `extra` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '如果是自增，则为auto_increment',
  `input_type` varchar(48) COLLATE utf8mb4_unicode_ci DEFAULT 'text' COMMENT '该字段的输入类型',
  `column_length` int DEFAULT NULL COMMENT '字段长度',
  `column_default` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '列默认值',
  `is_nullable` tinyint(1) DEFAULT '0' COMMENT '该列是否可为空',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `pub_codegen_tb_column_tb_name_index` (`table_name`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='表对应的column';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_codegen_table_column`
--

LOCK TABLES `pub_codegen_table_column` WRITE;
/*!40000 ALTER TABLE `pub_codegen_table_column` DISABLE KEYS */;
INSERT INTO `pub_codegen_table_column` (`id`, `table_id`, `table_name`, `column_name`, `member_name`, `data_type`, `member_type`, `query_type`, `column_key`, `column_comment`, `extra`, `input_type`, `column_length`, `column_default`, `is_nullable`, `update_time`, `create_time`) VALUES (1,1,'t_art','id','id','int','java.lang.Integer','=','PRI','主键',NULL,'text',11,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(2,2,'m_demand','id','id','varchar','java.lang.String','=','PRI','主键',NULL,'text',20,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(3,2,'m_demand','demand_number','demandNumber','varchar','java.lang.String','like',NULL,'需求编号',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(4,2,'m_demand','project_name','projectName','varchar','java.lang.String','like',NULL,'项目名称',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(5,2,'m_demand','project_type','projectType','varchar','java.lang.String','=',NULL,'项目类型',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(6,2,'m_demand','project_description','projectDescription','varchar','java.lang.String','like',NULL,'项目描述',NULL,'text',256,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(7,2,'m_demand','special_work','specialWork','varchar','java.lang.String','=',NULL,'所属专项工作/智慧供应链/数据字典',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(8,2,'m_demand','demand_name','demandName','varchar','java.lang.String','like',NULL,'申请业扩减容、销户容量/企业多维信用画像/需求名称/数字产品名称',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(9,2,'m_demand','data_description','dataDescription','varchar','java.lang.String','like',NULL,'所需数据描述',NULL,'text',512,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(10,2,'m_demand','data_usage','dataUsage','varchar','java.lang.String','like',NULL,'数据用途',NULL,'text',512,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(11,2,'m_demand','demand_department','demandDepartment','varchar','java.lang.String','like',NULL,'需求部门/所属业务部门/需求提出单位',NULL,'text',512,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(12,2,'m_demand','raised_date','raisedDate','varchar','java.lang.String','like',NULL,'需求提出日期',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(13,2,'m_demand','modified_date','modifiedDate','varchar','java.lang.String','like',NULL,'需求修改日期',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(14,2,'m_demand','submit_user_id','submitUserId','varchar','java.lang.String','like',NULL,'提报用户ID',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(15,2,'m_demand','submit_username','submitUsername','varchar','java.lang.String','like',NULL,'需求提报人',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(16,2,'m_demand','submit_user_phone','submitUserPhone','varchar','java.lang.String','like',NULL,'联系方式',NULL,'text',36,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(17,2,'m_demand','submit_user_email','submitUserEmail','varchar','java.lang.String','like',NULL,'邮箱',NULL,'text',36,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(18,2,'m_demand','demand_side_system','demandSideSystem','varchar','java.lang.String','like',NULL,'需求方系统',NULL,'text',36,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(19,2,'m_demand','deployment_mode','deploymentMode','varchar','java.lang.String','like',NULL,'需求方系统部署方式',NULL,'text',36,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(20,2,'m_demand','data_sharing_mode','dataSharingMode','varchar','java.lang.String','like',NULL,'数据共享方式/数据字典，支持多选',NULL,'text',36,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(21,2,'m_demand','demand_type','demandType','varchar','java.lang.String','like',NULL,'需求类型/数据字典，支持多选',NULL,'text',36,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(22,2,'m_demand','data_sharing_scope','dataSharingScope','varchar','java.lang.String','like',NULL,'数据共享范围/数据字典，支持多选',NULL,'text',256,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(23,2,'m_demand','data_sharing_frequency','dataSharingFrequency','varchar','java.lang.String','like',NULL,'数据共享频率/需求频度',NULL,'text',256,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(24,2,'m_demand','data_supply_department','dataSupplyDepartment','varchar','java.lang.String','like',NULL,'数据提供部门',NULL,'text',256,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(25,2,'m_demand','desired_time','desiredTime','varchar','java.lang.String','like',NULL,'希望实现时间',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(26,2,'m_demand','demand_status','demandStatus','varchar','java.lang.String','like',NULL,'需求状态',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(27,2,'m_demand','current_process','currentProcess','varchar','java.lang.String','=',NULL,'当前环节',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(28,2,'m_demand','remark','remark','varchar','java.lang.String','like',NULL,'备注信息',NULL,'text',512,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(29,3,'m_data_table','id','id','varchar','java.lang.String','=','PRI','主键',NULL,'text',20,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(30,3,'m_data_table','demand_id','demandId','varchar','java.lang.String','=','','需求ID',NULL,'text',20,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(31,3,'m_data_table','data_sys_name','dataSysName','varchar','java.lang.String','like','','数据来源系统名称',NULL,'text',48,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(32,3,'m_data_table','deployment_mode','deploymentMode','varchar','java.lang.String','like','','来源系统部署方式',NULL,'text',48,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(33,3,'m_data_table','data_range','dataRange','varchar','java.lang.String','like','','所需数据范围',NULL,'text',48,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(34,3,'m_data_table','data_type','dataType','varchar','java.lang.String','like',NULL,'数据类型',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(35,3,'m_data_table','table_name','tableName','varchar','java.lang.String','like',NULL,'源端系统对应数据表名',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(36,3,'m_data_table','table_description','tableDescription','varchar','java.lang.String','like',NULL,'源端系统数据表中文名/表描述',NULL,'text',512,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(37,3,'m_data_table','column_name','columnName','varchar','java.lang.String','like',NULL,'字段名称',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(38,3,'m_data_table','column_name_zh','columnNameZh','varchar','java.lang.String','like',NULL,'字段中文名',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(39,3,'m_data_table','column_type','columnType','varchar','java.lang.String','like',NULL,'字段类型',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(40,3,'m_data_table','remark','remark','varchar','java.lang.String','like',NULL,'备注',NULL,'text',512,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(41,4,'m_process_log','id','id','varchar','java.lang.String','=','PRI','主键',NULL,'text',20,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(42,4,'m_process_log','demand_id','demandId','varchar','java.lang.String','=',NULL,'需求id',NULL,'text',20,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(43,4,'m_process_log','current_process','currentProcess','varchar','java.lang.String','=',NULL,'当前流程',NULL,'text',20,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(44,4,'m_process_log','prev_id','prevId','varchar','java.lang.String','=',NULL,'上一个流程id',NULL,'text',20,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(45,4,'m_process_log','charge_id','chargeId','varchar','java.lang.String','=',NULL,'负责人',NULL,'text',20,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(46,4,'m_process_log','assignor_id','assignorId','varchar','java.lang.String','=',NULL,'指派人',NULL,'text',20,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(47,4,'m_process_log','is_in_resource_dir','isInResourceDir','varchar','java.lang.String','=',NULL,'是否在资源目录',NULL,'text',2,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(48,4,'m_process_log','resource_dir_loc','resourceDirLoc','varchar','java.lang.String','=',NULL,'资源目录位置',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(49,4,'m_process_log','data_traceability_flag','dataTraceabilityFlag','varchar','java.lang.String','=',NULL,'数据溯源及完善',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(50,4,'m_process_log','is_covered','isCovered','varchar','java.lang.String','=',NULL,'是否覆盖',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(51,4,'m_process_log','model_optimization_flag','modelOptimizationFlag','varchar','java.lang.String','=',NULL,'模型优化',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(52,4,'m_process_log','data_access_flag','dataAccessFlag','varchar','java.lang.String','=',NULL,'数据接入',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(53,4,'m_process_log','business_carding_flag','businessCardingFlag','varchar','java.lang.String','=',NULL,'业务梳理',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(54,4,'m_process_log','data_desensitization_flag','dataDesensitizationFlag','varchar','java.lang.String','=',NULL,'数据脱敏',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(55,4,'m_process_log','permission_assignment_flag','permissionAssignmentFlag','varchar','java.lang.String','=',NULL,'权限分配',NULL,'text',4,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(56,4,'m_process_log','data_share_flag','dataShareFlag','varchar','java.lang.String','=',NULL,'数据共享',NULL,'text',4,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(57,4,'m_process_log','rollback_flag','rollbackFlag','varchar','java.lang.String','=',NULL,'任务回退标记',NULL,'text',4,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(58,4,'m_process_log','attachments','attachments','varchar','java.lang.String','=',NULL,'过程附件',NULL,'text',48,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(59,4,'m_process_log','completion_time','completionTime','varchar','java.lang.String','=',NULL,'完成时间',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(60,5,'m_question_feedback','id','id','varchar','java.lang.String','=','PRI','主键',NULL,'text',20,NULL,0,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(61,5,'m_question_feedback','demand_id','demandId','varchar','java.lang.String','=',NULL,'需求ID',NULL,'text',20,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(62,5,'m_question_feedback','question_name','questionName','varchar','java.lang.String','like',NULL,'问题名称',NULL,'text',256,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(63,5,'m_question_feedback','question_description','questionDescription','varchar','java.lang.String','like',NULL,'问题描述',NULL,'text',256,NULL,1,'2020-06-23 21:23:59','2020-04-08 09:51:50'),(64,5,'m_question_feedback','user_id','userId','varchar','java.lang.String','like',NULL,'用户ID',NULL,'text',20,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(65,5,'m_question_feedback','username','username','varchar','java.lang.String','like',NULL,'问题提报人姓名',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(66,5,'m_question_feedback','telephone','telephone','varchar','java.lang.String','like',NULL,'提报人联系电话',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(67,5,'m_question_feedback','email','email','varchar','java.lang.String','like',NULL,'提报人邮箱',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(68,5,'m_question_feedback','submit_time','submitTime','varchar','java.lang.String','like',NULL,'问题提出时间',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(69,5,'m_question_feedback','update_time','updateTime','varchar','java.lang.String','like',NULL,'问题修改时间',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(70,5,'m_question_feedback','summary_charge_id','summaryChargeId','varchar','java.lang.String','like',NULL,'问题汇总负责人',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(71,5,'m_question_feedback','summary_assignor_id','summaryAssignorId','varchar','java.lang.String','like',NULL,'问题汇总指派人',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(72,5,'m_question_feedback','solving_charge_id','solvingChargeId','varchar','java.lang.String','like',NULL,'问题解决负责人',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(73,5,'m_question_feedback','solving_assignor_id','solvingAssignorId','varchar','java.lang.String','=',NULL,'问题解决指派人',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(74,5,'m_question_feedback','processing_mode','processingMode','varchar','java.lang.String','=',NULL,'问题处理方式',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(75,5,'m_question_feedback','processing_description','processingDescription','varchar','java.lang.String','like',NULL,'问题处理详情',NULL,'text',512,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(76,5,'m_question_feedback','processing_result','processingResult','varchar','java.lang.String','like',NULL,'问题处理结果',NULL,'text',512,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(77,5,'m_question_feedback','processing_time','processingTime','varchar','java.lang.String','like',NULL,'问题处理时间',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(78,5,'m_question_feedback','processing_user_id','processingUserId','varchar','java.lang.String','like',NULL,'处理用户ID',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(79,5,'m_question_feedback','remark','remark','varchar','java.lang.String','like',NULL,'备注',NULL,'text',512,NULL,1,'2020-06-23 21:23:58','2020-04-08 09:51:50'),(80,1,'t_art','name','Name','varchar','java.lang.String','like',NULL,'艺名',NULL,'text',48,NULL,1,'2020-06-23 21:23:58','2020-06-16 20:05:06'),(81,6,'t_you','you_id','YouId','int','java.lang.Long','=','PRI','主键id',NULL,'text',11,NULL,0,'2020-06-24 13:13:09','2020-06-24 13:13:09'),(82,6,'t_you','you_name','YouName','varchar','java.lang.String','=',NULL,'you名称',NULL,'text',128,NULL,1,'2020-06-24 13:13:45','2020-06-24 13:13:45'),(84,9,'t_me','id','','int','java.lang.Long','=','PRI','主键',NULL,'text',NULL,NULL,0,'2020-06-24 20:53:52','2020-06-24 20:53:52'),(85,9,'t_me','name','','varchar','java.lang.String','=','PRI','名称',NULL,'text',48,NULL,0,'2020-06-24 20:54:16','2020-06-24 20:54:16'),(86,9,'t_me','age','Age','int','int','>=',NULL,'年龄',NULL,'text',8,'0',1,'2020-06-24 21:26:35','2020-06-24 21:26:35');
/*!40000 ALTER TABLE `pub_codegen_table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_dictionary`
--

DROP TABLE IF EXISTS `pub_dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_dictionary` (
  `id` bigint NOT NULL,
  `pid` bigint DEFAULT NULL,
  `dict_path` varchar(64) DEFAULT NULL COMMENT '字典路径',
  `dict_level` tinyint NOT NULL DEFAULT '8',
  `dict_key` varchar(32) NOT NULL,
  `dict_value` varchar(32) DEFAULT NULL,
  `dict_name` varchar(64) DEFAULT NULL,
  `remark` varchar(128) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `pub_dictionary_pid_index` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_dictionary`
--

LOCK TABLES `pub_dictionary` WRITE;
/*!40000 ALTER TABLE `pub_dictionary` DISABLE KEYS */;
INSERT INTO `pub_dictionary` (`id`, `pid`, `dict_path`, `dict_level`, `dict_key`, `dict_value`, `dict_name`, `remark`, `create_time`, `update_time`) VALUES (123454321,128122434234,'0/0',2,'QZ','1','群众','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(128122434234,0,'0',1,'ZZMM','0','政治面貌','123','2019-08-30 12:03:26','2019-08-30 12:03:27'),(612456789098,128122434234,'0/1',2,'ZGDY','0','党员','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1123567890654,0,'1',1,'GENDER','3','性别','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1548488074236,0,'2',1,'Nation','2','民族','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1548488128913,1548488074236,'2/0',2,'Han','0213','汉族','Han 汉族','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1548493409868,1548488074236,'2/1',2,'Manchu','0214','满族','Manchu: 满族','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1548494110977,1548488074236,'2/2',2,'Zhuang','0215','壮族','Zhuang (壮族)','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1548494133746,1548488074236,'2/3',2,'Hui','0016','回族','Hui (回族)','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550029339934,1123567890654,'1/0',2,'MAN','1','男','Man:男','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550029408543,1123567890654,'1/1',2,'WOMAN','0','女','WOMAN:女','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550651548162,0,'3',1,'CESHI','4','测试数据','测试数据.可以删除','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550728376777,1550651548162,'3/0',2,'CESHI-1','0401','测试数据-1','测试数据','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550729131387,1550728376777,'3/0/0',3,'CESHI-1-0','10','测试数据-1-0','测试数据,描述层级','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550738519849,1548488074236,'2/4',2,'Miao','0017','苗族','Miao (苗族) ','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550738612644,1548488074236,'2/5',2,'Uighur','0018','维吾尔族','Uighur (维吾尔族)','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550738955769,1550728376777,'3/0/1',3,'CESHI-1-1','11','测试数据-1-1','测试数据','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550815906694,0,'4',1,'China','1','中国','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816005853,1550815906694,'4/0',2,'Beijing','110000','北京','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816084796,1550815906694,'4/1',2,'Tianjin','120000','天津','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816147172,1550815906694,'4/2',2,'Shanghai','310000','上海','','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816326538,1550815906694,'4/3',2,'Shandong','370000','山东','山东省 370000 SD','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816444869,1550816326538,'4/3/0',3,'Jinan','370100','济南','山东省 济南市','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816635348,1550816444869,'4/3/0/0',4,'LixiaQu','370101','历下区','山东省,济南市,历下区','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816764544,1550816326538,'4/3/1',3,'Qingdao','370200','青岛','山东省,青岛市','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816871433,1550816764544,'4/3/1/0',4,'ShixiaQu','370201','市辖区','山东省,青岛市,市辖区','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1550816993450,1550816147172,'4/2/0',3,'HuangpuQu','310101','黄浦区','上海市,黄浦区','2019-08-30 12:03:26','2019-08-30 12:03:27'),(1552463982598,1550815906694,'4/4',2,'Henan','410000000000','河南','河南省410000000000','2019-08-30 12:03:26','2019-08-30 12:03:27'),(558335635719454720,1550815906694,'4/5',2,'Hebei','130000000000','河北','河北省130000000000','2019-08-30 12:03:26','2019-08-30 12:03:27'),(571727506617401344,0,'5',1,'NEWS_TYPE','5','新闻类型','新闻类型','2019-08-30 12:03:26','2019-08-30 12:03:27'),(571729842341412864,571727506617401344,'5/0',2,'NEWS_TYPE_NEWS','0','新闻','新闻类型之新闻','2019-08-30 12:03:26','2019-08-30 12:03:27'),(571736777786654720,571727506617401344,'5/1',2,'NEWS_TYPE_ARTICLE','1','文章','新闻类型之文章','2019-08-30 12:03:26','2019-08-30 12:03:27');
/*!40000 ALTER TABLE `pub_dictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pub_dictionary_child_count_view`
--

DROP TABLE IF EXISTS `pub_dictionary_child_count_view`;
/*!50001 DROP VIEW IF EXISTS `pub_dictionary_child_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pub_dictionary_child_count_view` AS SELECT 
 1 AS `dict_id`,
 1 AS `child_num`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pub_dictionary_count_view`
--

DROP TABLE IF EXISTS `pub_dictionary_count_view`;
/*!50001 DROP VIEW IF EXISTS `pub_dictionary_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pub_dictionary_count_view` AS SELECT 
 1 AS `dict_id`,
 1 AS `dict_pid`,
 1 AS `child_num`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pub_file_upload`
--

DROP TABLE IF EXISTS `pub_file_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_file_upload` (
  `file_id` bigint NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `file_type` varchar(128) DEFAULT NULL COMMENT '文件类型：image/png;image/jpg;......',
  `file_path` varchar(512) NOT NULL COMMENT '文件路径',
  `file_status` smallint DEFAULT '1' COMMENT '文件状态',
  `uploader_id` bigint NOT NULL COMMENT '上传用户ID',
  `uploader_name` varchar(48) DEFAULT NULL COMMENT '上传用户名',
  `upload_time` datetime DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_file_upload`
--

LOCK TABLES `pub_file_upload` WRITE;
/*!40000 ALTER TABLE `pub_file_upload` DISABLE KEYS */;
INSERT INTO `pub_file_upload` (`file_id`, `file_name`, `file_type`, `file_path`, `file_status`, `uploader_id`, `uploader_name`, `upload_time`) VALUES (1,'test','image/png','/',1,1,'test','2019-10-22 16:46:07');
/*!40000 ALTER TABLE `pub_file_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_menu`
--

DROP TABLE IF EXISTS `pub_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_menu` (
  `menu_id` bigint NOT NULL,
  `menu_pid` bigint DEFAULT NULL,
  `menu_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '可重写Url,如果存在，那么就不适用resource对应的url',
  `menu_path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_level` tinyint DEFAULT NULL,
  `menu_name` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `menu_type` tinyint DEFAULT NULL,
  `menu_order` tinyint DEFAULT NULL,
  `menu_icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`menu_id`),
  KEY `pub_menu_menu_pid_index` (`menu_pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_menu`
--

LOCK TABLES `pub_menu` WRITE;
/*!40000 ALTER TABLE `pub_menu` DISABLE KEYS */;
INSERT INTO `pub_menu` (`menu_id`, `menu_pid`, `menu_url`, `menu_path`, `menu_level`, `menu_name`, `menu_type`, `menu_order`, `menu_icon`, `open_type`, `remark`, `create_time`, `update_time`) VALUES (101234569874125362,200000000000000000,'/users','0/1',2,'用户管理',0,1,'fa fa-user','a','用户管理菜单','2019-08-30 12:01:15','2020-07-10 16:07:13'),(111111111111111111,200000000000000000,'/role','0/2',2,'角色管理',0,2,'fa fa-users','a','角色管理','2019-08-30 12:01:15','2020-07-09 16:38:37'),(122222222222222222,200000000000000000,'/authorities','0/3',2,'权限管理',0,3,'fa fa-bomb','a','权限管理 （菜单）','2019-08-30 12:01:15','2020-07-10 17:09:12'),(155065417234388888,0,NULL,'3',1,'邮件管理',0,5,'fa fa-envelope-square','a','邮件管理一级菜单','2019-08-30 12:01:15','2020-07-09 16:38:37'),(155065483112000000,155065417234388888,'/mail/outbox','3/0',2,'发件箱',0,5,'fa fa-envelope-o','a','邮件管理-发件箱（二级菜单）','2019-08-30 12:01:15','2020-07-09 16:38:37'),(155073071609300000,155065417234388888,'/mail/inbox','3/1',2,'收件箱',0,2,'fa fa-envelope','a','','2019-08-30 12:01:15','2020-07-09 16:38:37'),(155107571356400000,155065417234388888,'/mail/draftsBox','3/2',2,'草稿箱',0,3,'fa fa-paper-plane','a','邮件管理-草稿箱 （二级菜单）','2019-08-30 12:01:15','2020-07-09 16:38:36'),(200000000000000000,0,NULL,'0',1,'系统管理',0,0,'fa fa-desktop','a','','2019-08-30 12:01:15','2020-07-09 16:38:36'),(200002000000000000,200800000000000000,NULL,'1/0/0',3,'测试用例1',0,1,NULL,'a',NULL,'2019-08-30 12:01:15','2019-08-30 12:02:11'),(200010000000000000,200800000000000000,NULL,'1/0/1',3,'测试用例2',0,2,NULL,'a',NULL,'2019-08-30 12:01:15','2019-08-30 12:02:11'),(200800000000000000,400000000100000000,NULL,'1/0',2,'二级菜单1',0,1,NULL,'a',NULL,'2019-08-30 12:01:15','2019-08-30 12:02:11'),(201600000000000000,400000000100000000,NULL,'1/1',2,'二级菜单2',0,2,NULL,'a',NULL,'2019-08-30 12:01:15','2019-08-30 12:02:11'),(202000000000000000,201600000000000000,'/testsocket/init','1/1/0',3,'测试用例3',0,1,'fa fa-calendar-minus-o','a','测试测试','2019-08-30 12:01:15','2020-07-09 16:38:37'),(300000000000000000,400000000100000000,NULL,'1/2',2,'四级横向菜单',0,2,NULL,'a',NULL,'2019-08-30 12:01:15','2019-08-30 12:02:11'),(300000000000002000,200000000000000000,'/datadictionary','0/6',2,'数据字典',0,8,'fa fa-cubes','a','数据字典','2019-08-30 12:01:15','2020-07-09 16:38:37'),(300000030000000000,300003000000000000,'/test/test','1/2/0/0/0',4,'测试用例4',0,0,'fa fa-hand-lizard-o','a','测试菜单数据','2019-08-30 12:01:15','2020-07-09 16:38:37'),(300003000000000000,300800000000000000,NULL,'1/2/0/0',3,'二级菜单',0,1,NULL,'a','测试菜单','2019-08-30 12:01:15','2019-08-30 12:02:11'),(300013000000000000,300800000000000000,'/test/test5','1/2/0/1',3,'测试用例5',0,4,NULL,'a','菜单测试用例','2019-08-30 12:01:15','2020-07-09 16:38:36'),(300800000000000000,300000000000000000,NULL,'1/2/0',2,'三级菜单',0,0,NULL,'a','三级菜单测试','2019-08-30 12:01:15','2019-08-30 12:02:11'),(301800000000000000,300000000000000000,'/test/test6','1/2/1',2,'测试用例6',0,2,NULL,'a','测试用例6 可以删除','2019-08-30 12:01:15','2020-07-09 16:38:36'),(400000000000000000,200000000000000000,'/org','0/0',2,'机构管理',0,5,'fa fa-object-ungroup','a','后台管理:机构管理','2019-08-30 12:01:15','2020-07-09 16:38:37'),(400000000100000000,0,NULL,'1',1,'三级菜单',0,1,NULL,'a',NULL,'2019-08-30 12:01:15','2019-08-30 12:02:11'),(500000000000000000,200000000000000000,'/resource','0/4',2,'资源管理',0,4,'fa fa-gg','a','资源管理  资源数','2019-08-30 12:01:15','2020-07-09 16:38:37'),(500000000000000001,200000000000000000,'/sysparam','0/5',2,'参数管理',0,7,'fa fa-star-o','a','参数管理','2019-08-30 12:01:15','2020-07-09 16:38:37'),(500000000000000002,200000000000000000,'/menu','0/10',2,'菜单管理',0,4,'fa fa-tasks','a','资源管理  资源数','2019-08-30 12:01:15','2020-07-09 16:38:37'),(562583830184067072,0,NULL,'5',1,'日志管理',0,4,'fa fa-clipboard','a','管理日志信息','2019-08-30 12:01:15','2020-07-09 16:38:37'),(562584246460350464,562583830184067072,'/oplog','5/0',2,'操作日志',0,0,'fa fa-file-o','a','操作日志左侧日志菜单','2019-08-30 12:01:15','2020-07-09 16:38:37'),(562665103883763712,155065417234388888,'/mail/createMail','3/3',2,'发邮件',2,6,'fa fa-paper-plane-o','a','邮件管理-发邮件','2019-08-30 12:01:15','2020-06-04 21:49:01'),(614760704813039616,300000000000000000,'/test/test7','1/2/1/2',2,'测试菜单7',0,5,NULL,'a','测试菜单','2019-08-30 12:01:15','2020-07-09 16:38:49'),(614851327616352256,0,'','7',1,'演示案例',0,8,'fa fa-youtube-square','i','演示案例','2019-08-30 12:01:15','2019-10-31 17:29:29'),(614851970376663040,614851327616352256,'/demos/examples/icon','7/0',2,'图标演示',0,0,'fa fa-smile-o','i','图标演示','2019-08-30 12:01:15','2019-10-29 12:33:17'),(614856071072186368,614851327616352256,'/demos/examples/button','7/1',2,'按钮演示',0,1,'fa fa-square','i','按钮演示','2019-08-30 12:01:15','2019-10-29 12:34:18'),(615542306333786112,658694878313578496,'','9/1',2,'系统工具',0,9,'fa fa-wrench','a','系统工具：代码生成、自动部署','2019-08-30 12:01:15','2019-12-23 15:55:24'),(615542600622931968,615542306333786112,'/systables','9/1/0',3,'代码生成',0,0,'fa fa-code','a','代码生成','2019-08-30 12:01:15','2020-04-08 14:42:14'),(615543341039222784,615542306333786112,'/automatic/deploy','9/1/1',3,'自动部署',0,1,'fa fa-upload','a','自动部署war包','2019-08-30 12:01:15','2020-07-09 16:39:00'),(615843847246708736,200000000000000000,'/files/init','0/9',2,'文件管理',0,10,'fa fa-folder-o','a','系统管理》文件管理','2019-08-30 12:01:15','2020-07-09 16:39:00'),(615960495727312896,0,'','8',1,'内容管理',0,7,'fa fa-files-o','a','内容管理，包括文章管理，新闻模块，帖子模块等','2019-08-30 12:01:15','2019-11-04 16:50:25'),(615960919121330176,615960495727312896,'','8/0',2,'新闻模块',0,0,'fa fa-file-text-o','a','新闻模块','2019-08-30 12:01:15','2019-11-04 16:50:46'),(616637194316021760,614851327616352256,'/demos/examples/image','7/2',2,'图片演示',0,0,'fa fa-picture-o','i','演示案例》图片演示','2019-08-30 12:01:15','2019-08-31 18:16:38'),(616949089816281088,615960495727312896,'','8/1',2,'文章模块',0,0,'fa fa-file-text','a','文章模块管理','2019-08-30 12:01:15','2019-10-31 12:56:28'),(617037398957621248,614851327616352256,'/demos/examples/video','7/3',2,'视频演示',0,0,'fa fa-caret-square-o-right','i','视频演示','2019-08-30 16:46:20','2019-08-31 18:16:38'),(618031317774237696,201600000000000000,'','1/1/1',3,'测试用例4',0,0,'fa fa-bullhorn','a','测试案例','2019-09-02 10:35:48','2019-09-02 10:35:48'),(626790266698727424,615542306333786112,'/automatic/restful','9/1/2',3,'接口测试',0,4,'fa fa-indent','i','接口测试','2019-09-26 14:40:44','2019-12-23 15:55:24'),(636494305011171328,614851327616352256,'/demos/examples/markdown','7/5',2,'Markdown演示',0,0,'fa fa-file-text-o','i','markdown 演示信息','2019-10-23 09:21:07','2019-10-23 09:21:07'),(638710778181976064,614851327616352256,'/demos/examples/qrcode','7/6',2,'qrcode演示',0,0,'fa fa-qrcode','i','二维码演示及生成','2019-10-29 12:08:36','2020-05-31 20:12:33'),(658694878313578496,0,'','9',1,'工具',0,0,'fa fa-cog','a','提供一系列工具服务','2019-12-23 15:38:17','2020-04-27 09:46:51'),(658695455152013312,658694878313578496,'/tools/formatJson','9/0',2,'JSON格式化',0,0,'fa fa-cube','i','json格式化工具','2019-12-23 15:40:34','2019-12-25 12:14:11'),(658697966831271936,614851327616352256,'/demos/examples/fileUpload','7/7',2,'文件上传',0,0,'fa fa-cloud-upload','i','测试文件上传','2019-12-23 15:50:33','2020-05-31 20:12:33'),(703536779973623808,658694878313578496,'/automatic/form','9/2',2,'表单构建',0,0,'fa fa-paper-plane-o','a','通过拖拽构造表单','2020-04-25 09:23:59','2020-04-26 16:18:29'),(704069628095102976,658694878313578496,'','9/3',2,'系统服务参数',0,4,'fa fa-user-cog','a','系统服务参数','2020-04-26 20:41:20','2020-04-27 09:46:03'),(716249406478745600,562583830184067072,'','5/1',2,'登录日志',0,0,'fa fa-align-justify','a','登录日志','2020-05-30 11:19:25','2020-05-30 11:19:25'),(724023642530250752,562583830184067072,'/versions','5/2',2,'版本日志',0,0,'fa fa-shield','a','管理系统版本更新日志','2020-06-20 22:11:28','2020-06-20 22:23:47'),(725037605556584448,200000000000000000,'/noticeinfo/noticeInfoInit','0/11',2,'通知公告',0,0,'fa fa-paper-plane-o','a','通知公告','2020-06-23 17:20:35','2020-06-23 17:22:31'),(764869176442486784,0,'','10',1,'微信',0,0,'fa fa-wechat','a','微信菜单','2020-10-11 15:17:02','2020-10-11 15:17:02');
/*!40000 ALTER TABLE `pub_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_menu_authority`
--

DROP TABLE IF EXISTS `pub_menu_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_menu_authority` (
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  `authority_id` bigint NOT NULL COMMENT '权限ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`menu_id`,`authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_menu_authority`
--

LOCK TABLES `pub_menu_authority` WRITE;
/*!40000 ALTER TABLE `pub_menu_authority` DISABLE KEYS */;
INSERT INTO `pub_menu_authority` (`menu_id`, `authority_id`, `update_time`, `create_time`) VALUES (101234569874125362,1377925320407,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(101234569874125362,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(111111111111111111,1377925329991,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(111111111111111111,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(122222222222222222,1377925337277,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(122222222222222222,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155065483112000000,1472450956478,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155065483112000000,1550653995378,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155065483112000000,1550730903140,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155073071609300000,1472450956478,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155073071609300000,1550730903140,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155073071609300000,1550730926998,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155107571356400000,1472450956478,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155107571356400000,1550730903140,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(155107571356400000,1551075655698,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(202000000000000000,1472450956478,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(202000000000000000,1472450971285,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(202000000000000000,1472450976416,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300000000000002000,1475891882706,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300000000000002000,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300000030000000000,1472450956478,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300000030000000000,1472450971285,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300000030000000000,1472450976416,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300013000000000000,1472450956478,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(300013000000000000,1472450971285,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(301800000000000000,1472450971285,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(400000000000000000,1377925254409,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(400000000000000000,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(500000000000000000,1377925351603,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(500000000000000000,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(500000000000000001,1385021957879,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(500000000000000001,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(500000000000000002,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(562584246460350464,562584829640572928,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(562665103883763712,1550730903140,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(614851970376663040,614852257971699712,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(615542600622931968,614401253123817472,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(615542600622931968,615958614380642304,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(615543341039222784,615958614380642304,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(615843847246708736,615844508373876736,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(626790266698727424,615958614380642304,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(658695455152013312,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(703536779973623808,615958614380642304,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(724023642530250752,562584829640572928,'2020-07-09 17:22:48','2020-07-09 17:22:48'),(725037605556584448,1475894194473,'2020-07-09 17:22:48','2020-07-09 17:22:48');
/*!40000 ALTER TABLE `pub_menu_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pub_menu_child_count_view`
--

DROP TABLE IF EXISTS `pub_menu_child_count_view`;
/*!50001 DROP VIEW IF EXISTS `pub_menu_child_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pub_menu_child_count_view` AS SELECT 
 1 AS `menu_id`,
 1 AS `child_num`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pub_menu_count_view`
--

DROP TABLE IF EXISTS `pub_menu_count_view`;
/*!50001 DROP VIEW IF EXISTS `pub_menu_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pub_menu_count_view` AS SELECT 
 1 AS `menu_id`,
 1 AS `menu_pid`,
 1 AS `child_num`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pub_notice_info`
--

DROP TABLE IF EXISTS `pub_notice_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_notice_info` (
  `notice_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(256) DEFAULT '' COMMENT '通知公告标题',
  `content` text COMMENT '通知公告内容',
  `notice_type` int DEFAULT '0' COMMENT '通知公告类型',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_time` datetime DEFAULT NULL COMMENT '通知公告发布时间',
  `deadline` datetime DEFAULT NULL COMMENT '通知公告过期时间',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='通知公告';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_notice_info`
--

LOCK TABLES `pub_notice_info` WRITE;
/*!40000 ALTER TABLE `pub_notice_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_notice_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_org`
--

DROP TABLE IF EXISTS `pub_org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_org` (
  `id` bigint NOT NULL,
  `pid` bigint NOT NULL COMMENT '父节点id',
  `org_path` varchar(64) DEFAULT NULL COMMENT '机构层级路径',
  `org_level` tinyint NOT NULL COMMENT '机构级别',
  `org_code` varchar(64) DEFAULT NULL,
  `org_name` varchar(64) DEFAULT NULL,
  `org_address` varchar(256) DEFAULT NULL COMMENT '机构地址',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否可用',
  `remark` varchar(256) DEFAULT NULL COMMENT '备注信息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `pub_org_pid_index` (`pid`),
  KEY `pub_org_org_path_index` (`org_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_org`
--

LOCK TABLES `pub_org` WRITE;
/*!40000 ALTER TABLE `pub_org` DISABLE KEYS */;
INSERT INTO `pub_org` (`id`, `pid`, `org_path`, `org_level`, `org_code`, `org_name`, `org_address`, `is_enabled`, `remark`, `create_time`, `update_time`) VALUES (1,0,'0',1,'0','根节点',NULL,1,'组织机构根节点，请勿删除！','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1475754962602,1,'0/0',2,'0','测试-0','中国',1,'测试修改org信息','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550130631664,1475754962602,'0/0/0',3,'00','测试-0-0','商丘',1,'测试新增org','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550131113241,1475754962602,'0/0/1',3,'01','测试-0-1','郑州',1,'测试新增org数据','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550131605508,1,'0/1',2,'1','测试-1','天津',1,'测试新增机构1','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550134807976,1550131605508,'0/1/0',3,'12','测试-1-2','烟台',1,'测试删除org','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649243906,1550131605508,'0/1/1',3,'13','测试-1-3','北京',1,'测试-1-3,测试信息,可以删除','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649293063,1550131605508,'0/1/2',3,'14','测试-1-4','北京',1,'测试org新增,可以删除','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649343046,1550649243906,'0/1/0/0',4,'130','测试-1-3-0','北京海淀',1,'测试多级树,可以删除','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649386179,1550130631664,'0/0/0/0',4,'000','测试-0-0-0','南京',1,'测试数据','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649433928,1550130631664,'0/0/0/1',4,'001','测试-0-0-1','西安',1,'测试数据,可以删除','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649478956,1,'0/2',2,'2','测试-2','青岛',1,'测试数据','2019-08-30 12:04:01','2019-08-30 12:04:02'),(1550649508145,1,'0/3',2,'3','测试-3','济南',1,'测试数据','2019-08-30 12:04:01','2019-08-30 12:04:02'),(562333828627038208,1550131605508,'0/1/3',3,'15','测试-1-5','测试-1-5',1,'测试数据123','2019-08-30 12:04:01','2019-08-30 12:04:02');
/*!40000 ALTER TABLE `pub_org` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pub_org_child_count_view`
--

DROP TABLE IF EXISTS `pub_org_child_count_view`;
/*!50001 DROP VIEW IF EXISTS `pub_org_child_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pub_org_child_count_view` AS SELECT 
 1 AS `org_id`,
 1 AS `child_num`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pub_org_count_view`
--

DROP TABLE IF EXISTS `pub_org_count_view`;
/*!50001 DROP VIEW IF EXISTS `pub_org_count_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pub_org_count_view` AS SELECT 
 1 AS `org_id`,
 1 AS `org_pid`,
 1 AS `child_num`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pub_project`
--

DROP TABLE IF EXISTS `pub_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_project` (
  `project_id` int NOT NULL AUTO_INCREMENT COMMENT '项目id',
  `creator_id` bigint NOT NULL COMMENT '创建者id',
  `creator_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建者账户',
  `project_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '项目名称',
  `project_description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '项目描述',
  `project_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '项目地址',
  `project_lang` varchar(48) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '编程语言',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_project`
--

LOCK TABLES `pub_project` WRITE;
/*!40000 ALTER TABLE `pub_project` DISABLE KEYS */;
INSERT INTO `pub_project` (`project_id`, `creator_id`, `creator_name`, `project_name`, `project_description`, `project_url`, `project_lang`, `modified_time`, `create_time`) VALUES (0,1,'admin','common','公共数据表','https://gitee.com/lixuebiao','All','2020-05-25 16:31:23','2020-05-25 16:27:33'),(1,1,'admin','hsrubberai','恒生塑胶管理系统','https://gitee.com/lixuebiao/hsrubberai','Java','2020-05-25 16:31:23','2019-11-06 16:09:10'),(2,1,'admin','weike','测试','https://gitee.com/lixuebiao/weike','Java','2020-05-25 16:31:23','2019-11-06 16:48:33'),(3,1,'admin','WeikeMgt','WeikeMgt','https://gitee.com/lixuebiao/WeikeMgt','Java','2020-05-25 16:31:23','2019-11-06 16:55:10'),(4,1,'admin','Test','测试添加数据','https://gitee.com/lixuebiao/Test','Java','2020-05-25 16:31:23','2019-11-06 16:56:06'),(5,1,'admin','Test2','测试项目2','https://gitee.com/lixuebiao/test2','Java','2020-05-25 16:31:23','2019-11-12 15:07:56'),(6,1550215149448,'test','Test2','# Test2项目','https://gitee.com/lixuebiao/test2','Java','2020-05-25 16:31:23','2019-11-29 13:37:37'),(7,1550215149448,'test','test3','`项目测试描述信息`','https://gitee.com/lixuebiao/test3','Typescript','2020-05-25 16:31:23','2019-12-31 14:22:35');
/*!40000 ALTER TABLE `pub_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_quartz`
--

DROP TABLE IF EXISTS `pub_quartz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_quartz` (
  `id` bigint NOT NULL COMMENT '主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_quartz`
--

LOCK TABLES `pub_quartz` WRITE;
/*!40000 ALTER TABLE `pub_quartz` DISABLE KEYS */;
/*!40000 ALTER TABLE `pub_quartz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_resource`
--

DROP TABLE IF EXISTS `pub_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_resource` (
  `resource_id` bigint NOT NULL,
  `resource_name` varchar(64) DEFAULT NULL,
  `resource_url` varchar(64) NOT NULL DEFAULT '' COMMENT '菜单对应的资源url,可为空',
  `resource_status` tinyint NOT NULL DEFAULT '1' COMMENT '资源状态',
  `resource_type` tinyint(1) DEFAULT '0' COMMENT '菜单类型 0：菜单 1：btn按钮',
  `open_type` char(1) DEFAULT 'a' COMMENT '打开方式',
  `remark` varchar(256) DEFAULT NULL COMMENT '菜单备注信息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_resource`
--

LOCK TABLES `pub_resource` WRITE;
/*!40000 ALTER TABLE `pub_resource` DISABLE KEYS */;
INSERT INTO `pub_resource` (`resource_id`, `resource_name`, `resource_url`, `resource_status`, `resource_type`, `open_type`, `remark`, `create_time`, `update_time`) VALUES (101234569874125362,'用户管理','/user',1,0,'a','用户管理菜单','2019-08-30 11:57:50','2019-08-30 11:57:51'),(111111111111111111,'角色管理','/role',1,0,'a','角色管理','2019-08-30 11:57:50','2019-08-30 11:57:51'),(122222222222222222,'权限管理','/authority',1,0,'a','权限管理 （菜单）','2019-08-30 11:57:50','2019-08-30 11:57:51'),(155065483112000000,'发件箱','/mail/outbox',1,0,'a','邮件管理-发件箱（二级菜单）','2019-08-30 11:57:50','2019-08-30 11:57:51'),(155073071609300000,'收件箱','/mail/inbox',1,0,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(155107571356400000,'草稿箱','/mail/draftsBox',1,0,'a','邮件管理-草稿箱 （二级菜单）','2019-08-30 11:57:50','2019-08-30 11:57:51'),(202000000000000000,'测试用例3','/testsocket/init',1,0,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(300000000000002000,'数据字典','/datadictionary',1,0,'a','数据字典','2019-08-30 11:57:50','2019-08-30 11:57:51'),(300000030000000000,'测试用例4','/test/test',1,0,'a','测试菜单数据','2019-08-30 11:57:50','2019-08-30 11:57:51'),(300013000000000000,'测试用例5','/test/test5',1,0,'a','菜单测试用例','2019-08-30 11:57:50','2019-08-30 11:57:51'),(301800000000000000,'测试用例6','/test/test6',1,0,'a','测试用例6 可以删除','2019-08-30 11:57:50','2019-08-30 11:57:51'),(400000000000000000,'机构管理','/org',1,0,'a','后台管理:机构管理','2019-08-30 11:57:50','2019-08-30 11:57:51'),(500000000000000000,'资源管理','/resource',1,0,'a','资源管理  资源数','2019-08-30 11:57:50','2019-08-30 11:57:51'),(500000000000000001,'参数管理','/sysparam',1,0,'a','参数管理','2019-08-30 11:57:50','2019-08-30 11:57:51'),(500000000000000002,'菜单管理','/menu',1,0,'a','菜单管理','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560131716690214912,'机构查询','/org/queryList',1,1,'a','系统管理:机构管理:机构查询','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560136929425752064,'机构更新','/org/updateOrg',1,1,'a','系统管理:机构管理:机构更新','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560144203586207744,'机构删除','/org/deleteAllById',1,1,'a','系统管理:机构管理:机构删除','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560144671087525888,'机构新增','/org/addOrg',1,1,'a','系统管理:机构管理:机构新增 操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560144842340958208,'机构树查看','/org/treePopWin',1,1,'a','系统管理:机构管理:机构树查看','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560145104581427200,'用户查询','/user/queryList',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560145187582509056,'用户更新','/user/updateUser',1,1,'a','用户更新 操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560145278183669760,'用户删除','/user/deleteAllById',1,1,'a','用户删除操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560145378968600576,'用户新增','/user/addUser',1,1,'a','用户新增 操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560145529888047104,'用户角色','/user/updateUserRole',1,1,'a','为用户分配角色 操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560146477360349184,'角色查询','/role/queryList',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560146544100114432,'角色更新','/role/updatePopWin',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560146596721852416,'角色新增','/role/addPopWin',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560146679995564032,'角色删除','/role/deleteAllById',1,1,'a','角色删除操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560146785939488768,'角色权限','/role/updateAuthPopWin',1,1,'a','为角色添加权限','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560147341567328256,'权限查询','/authority/queryList',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560147425969307648,'权限更新','/authority/updatePopWin',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560147501450002432,'权限新增','/authority/addPopWin',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560147545133678592,'权限删除','/authority/deleteAllById',1,1,'a','权限删除按钮','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560147677975674880,'权限资源','/authority/registerAuthMenuPopWin',1,1,'a','为权限注册资源','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560157692971188224,'资源新增','/resource/addMenu',1,1,'a','新增资源','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560158370531639296,'参数查询','/sysparam/queryList',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560158553654951936,'参数新增','/sysparam/addPopWin',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387114890428416,'字典查询','/datadictionary/queryList',1,1,'a','查询字典数据','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387355349876736,'资源删除','/resource/deleteById',1,1,'a','资源删除操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387452984885248,'资源更新','/resource/updateMenu',1,1,'a','资源更新操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387536724164608,'参数删除','/sysparam/deleteAllById',1,1,'a','参数删除描述','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387595398283264,'参数更新','/sysparam/updatePopWin',1,1,'a',NULL,'2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387658128293888,'字典新增','/datadictionary/addPopWin',1,1,'a','新增字典PopWin','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387714940141568,'字典删除','/datadictionary/deleteAllById',1,1,'a','字典删除操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560387771554856960,'字典更新','/datadictionary/updatePopWin',1,1,'a','字典更新PopWin','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560512300117655552,'个人信息','/user/loginMsg4Update',1,1,'a','','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560512489406595072,'修改密码','/user/updateLoginPwd',1,1,'a','只要用户登录即可拥有此权限','2019-08-30 11:57:50','2019-08-30 11:57:51'),(560512663063363584,'系统登录','/welcome',1,1,'a','登录后用户进入系统','2019-08-30 11:57:50','2019-08-30 11:57:51'),(561571852711886848,'查看字典树','/datadictionary/treePopWin',1,1,'a','查看字典树PopWin','2019-08-30 11:57:50','2019-08-30 11:57:51'),(562584246460350464,'操作日志','/oplog',1,0,'a','操作日志左侧日志菜单','2019-08-30 11:57:50','2019-08-30 11:57:51'),(562584572999499776,'日志删除','/oplog/deleteLogs',1,1,'a','删除日志操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(562665103883763712,'发邮件','/mail/addPopWin',1,1,'a','邮件管理-发邮件','2019-08-30 11:57:50','2019-08-30 11:57:51'),(566670610814468096,'重置密码','/user/resetUserPwd',1,1,'a','重置密码操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(572021254354108416,'新建新闻','/admin/news/create',1,1,'a','后台管理》新建新闻操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(614760704813039616,'测试菜单7','/test/test7',1,0,'a','测试菜单','2019-08-30 11:57:50','2019-08-30 11:57:51'),(614856071072186368,'按钮演示','/demos/examples/button',1,0,'i','按钮演示','2019-08-30 11:57:50','2019-08-30 11:57:51'),(615542600622931968,'代码生成','/systables',1,0,'a','代码生成','2019-08-30 11:57:50','2020-04-08 14:42:14'),(615542922468655104,'表查询','/systables/queryList',1,1,'a','查询表','2019-08-30 11:57:50','2020-04-08 14:42:40'),(615543151809003520,'生成代码','/systables/code',1,1,'a','根据条件生成代码','2019-08-30 11:57:50','2020-04-08 14:42:52'),(615543341039222784,'自动部署','/automatic/deploy',1,0,'a','自动部署war包','2019-08-30 11:57:50','2019-08-30 11:57:51'),(615843847246708736,'文件管理','/files/init',1,0,'a','系统管理》文件管理','2019-08-30 11:57:50','2019-08-30 11:57:51'),(615844208279814144,'文件查询','/files/list',1,1,'a','文件管理》文件查询','2019-08-30 11:57:50','2019-08-30 11:57:51'),(615961211892137984,'新闻管理','/admin/news',1,0,'a','内容管理》新闻模块》新闻管理','2019-08-30 11:57:50','2019-08-30 11:57:51'),(616600222251876352,'演示案例','/demos/examples/**',1,0,'a','演示案例，demos等请求','2019-08-30 11:57:50','2019-08-30 11:57:51'),(616961339344027648,'菜单删除','/menu/deleteAllById',1,1,'a','删除菜单操作','2019-08-30 11:57:50','2019-08-30 11:57:51'),(626789773670875136,'接口测试','/automatic/restful',1,0,'i','接口测试页面','2019-09-26 14:38:47','2019-09-26 14:38:47'),(658696593024745472,'工具','/tools/**',1,0,'a','系统提供的一系列工具','2019-12-23 15:45:05','2019-12-23 15:45:38'),(703540575126683648,'自动化','/automatic/**',1,2,'a','自动化工具','2020-04-25 09:39:04','2020-04-25 09:39:04'),(724026554878787584,'版本查询','/versions/queryList',1,0,'a','版本查询信息','2020-06-20 22:23:02','2020-06-20 22:23:02'),(725037935178547200,'通知公告','/noticeinfo/noticeInfoQueryList',1,2,'a','通知公告','2020-06-23 17:21:54','2020-06-23 17:21:54');
/*!40000 ALTER TABLE `pub_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_role`
--

DROP TABLE IF EXISTS `pub_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_role` (
  `role_id` bigint NOT NULL,
  `role_name` varchar(64) NOT NULL,
  `role_key` varchar(128) DEFAULT NULL COMMENT '角色key,跟authority_key类似',
  `role_level` tinyint DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `remark` varchar(128) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_role`
--

LOCK TABLES `pub_role` WRITE;
/*!40000 ALTER TABLE `pub_role` DISABLE KEYS */;
INSERT INTO `pub_role` (`role_id`, `role_name`, `role_key`, `role_level`, `is_enabled`, `remark`, `create_time`, `update_time`) VALUES (377351043380,'超级管理员','super-admin',1,1,'最牛逼的角色','2019-08-30 11:55:44','2020-07-09 11:47:59'),(400838519752,'一级管理员','l1-admin',2,1,'比最牛逼的角色略逊一筹的角色','2019-08-30 11:55:49','2020-07-09 11:47:59'),(400838540878,'二级管理员','l2-admin',3,1,'比一级管理员差一点的角色','2019-08-30 11:55:52','2020-07-09 11:47:59'),(1550481293329,'测试角色1','test1',2,1,'仅仅测试,可以删除','2019-08-30 11:55:55','2020-07-09 11:47:59'),(1550649885862,'测试角色2','test2',2,1,'测试角色,可以删除','2019-08-30 11:55:57','2020-07-09 11:47:59'),(561551221077311488,'测试角色3','test3',1,1,'测试数据，角色，可以删除3','2019-08-30 11:55:58','2020-07-09 11:47:59'),(566658822970540032,'测试角色4','test4',0,1,'测试角色4','2019-08-30 11:55:59','2020-07-09 11:47:59');
/*!40000 ALTER TABLE `pub_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_role_authority`
--

DROP TABLE IF EXISTS `pub_role_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_role_authority` (
  `role_id` bigint NOT NULL,
  `authority_id` bigint NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`role_id`,`authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_role_authority`
--

LOCK TABLES `pub_role_authority` WRITE;
/*!40000 ALTER TABLE `pub_role_authority` DISABLE KEYS */;
INSERT INTO `pub_role_authority` (`role_id`, `authority_id`, `create_time`, `update_time`) VALUES (377351043380,1377925320407,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,1377925337277,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,1475891882706,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,1475894194473,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,1550730903140,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,562584829640572928,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,572020202649157632,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,614852257971699712,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,615844508373876736,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(377351043380,615958614380642304,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(400838519752,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(400838540878,1377925254409,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(400838540878,1377925320407,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(400838540878,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(1550481293329,1377925337277,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(1550481293329,1472450956478,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(1550481293329,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(1550481293329,1550653995378,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(1550649885862,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(561551221077311488,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(561551221077311488,1550730903140,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(561551221077311488,562584829640572928,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(566658822970540032,1472450956478,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(566658822970540032,1475889178590,'2019-08-30 12:04:23','2019-08-30 12:04:23'),(566658822970540032,562584829640572928,'2019-08-30 12:04:23','2019-08-30 12:04:23');
/*!40000 ALTER TABLE `pub_role_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_sys_param`
--

DROP TABLE IF EXISTS `pub_sys_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_sys_param` (
  `param_id` bigint NOT NULL,
  `param_name` varchar(64) NOT NULL,
  `param_key` varchar(32) NOT NULL,
  `param_value` varchar(64) DEFAULT NULL,
  `param_value_type` varchar(8) NOT NULL DEFAULT 'STRING',
  `param_status` char(1) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_sys_param`
--

LOCK TABLES `pub_sys_param` WRITE;
/*!40000 ALTER TABLE `pub_sys_param` DISABLE KEYS */;
INSERT INTO `pub_sys_param` (`param_id`, `param_name`, `param_key`, `param_value`, `param_value_type`, `param_status`, `create_time`, `update_time`) VALUES (124190054819418694,'系统邮箱','SYSTEM_MAIL','test2@126.com','EMAIL','1','2019-08-30 12:04:37','2019-08-30 12:04:38'),(151753864288812324,'登录失败错误次数','LOGIN_ERROR_TIMES','3','NUMBER','1','2019-08-30 12:04:37','2019-08-30 12:04:38'),(154785574174623959,'通信加密密钥','SOCK-KEY','1234567890abcdef0abcdef123456789','STRING','1','2019-08-30 12:04:37','2019-08-30 12:04:38'),(186123654789654566,'短信密钥','API-KEY','b832f01bb37905df13affa7a93350b31','STRING','1','2019-08-30 12:04:37','2019-08-30 12:04:38'),(199852963214568755,'测试参数1','TEST_1','csh1','STRING','1','2019-08-30 12:04:37','2019-08-30 12:04:38'),(285012843988104206,'默认文件存储文件夹','DEFAULT_FILE_FOLDER','d:/testfileupload','PATH','1','2019-08-30 12:04:37','2019-08-30 12:04:38'),(555418837906882560,'测试参数2','TEST_2','test22222','STRING','1','2019-08-30 12:04:37','2019-08-30 12:04:38');
/*!40000 ALTER TABLE `pub_sys_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_user`
--

DROP TABLE IF EXISTS `pub_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_user` (
  `id` bigint NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `nickname` varchar(32) NOT NULL DEFAULT '' COMMENT '昵称',
  `realname` varchar(64) DEFAULT '' COMMENT '真实姓名',
  `avatar` varchar(128) DEFAULT '' COMMENT '用户头像',
  `gender` tinyint(1) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `org_id` bigint DEFAULT NULL,
  `org_name` varchar(128) DEFAULT NULL COMMENT '对应org_id',
  `duty` varchar(32) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `qq` varchar(32) DEFAULT NULL COMMENT 'qq号',
  `weixin` varchar(32) DEFAULT NULL COMMENT '微信号',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `description` varchar(128) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `avatar_id` varchar(48) DEFAULT NULL COMMENT 'avatarId 头像id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_user`
--

LOCK TABLES `pub_user` WRITE;
/*!40000 ALTER TABLE `pub_user` DISABLE KEYS */;
INSERT INTO `pub_user` (`id`, `username`, `password`, `nickname`, `realname`, `avatar`, `gender`, `birthday`, `org_id`, `org_name`, `duty`, `telephone`, `email`, `qq`, `weixin`, `is_enabled`, `description`, `create_time`, `update_time`, `avatar_id`) VALUES (1,'admin','{bcrypt}$2a$10$mQunqei4d3D9GKcsmtnUzuP2ZKjUXNkY1UCSaNrzTKo1vKFVMLZFq','管理员','超级管理员','',1,'2013-11-18',1,'根节点','超级超级管理员','10086','admin@admin.com','111111',NULL,1,'最牛逼的管理员','2019-08-30 12:04:56','2020-04-26 09:43:16',NULL),(1526264598235,'13361255388','{bcrypt}$2a$10$kkAYTfyGWFQHkVPRzK2JceSs3BFvIMPU0IGaW93.iUA2pH5Vqk1DO','13361255388','管理员','',1,'2000-11-18',1,'根节点','技术总监','13361255388','zhang@163.com','222222',NULL,1,'测试更新数据','2019-08-30 12:04:56','2020-04-26 09:43:16',NULL),(1550215149448,'test1','{bcrypt}$2a$10$FsK7YKw1aGF7HD4Xn7vG0.6LJG5Z76fxbzNNOGpaeYZDALVaHfNzO','test1','悟空','',1,'2019-02-07',1550130631664,'测试-0-0','java develeper','13465525937','test1@163.com','333333',NULL,1,'','2019-08-30 12:04:56','2020-04-26 09:43:16',NULL),(1550469685011,'test2','{bcrypt}$2a$10$RrsWjfDfRrXfUy6/aVsSQ.h4ZeFUnx0m5QHljETCb72jZdVOBAucS','test2','宋江','',1,'2019-01-27',1550131605508,'测试-1','测试员2','14576589862','test2@mm.com','232323',NULL,1,'','2019-08-30 12:04:56','2020-04-26 09:42:11',NULL),(1550649666525,'test3','{bcrypt}$2a$10$GT415Tmfu49/k.JW6v1aw.i2dMRAg3OQq.mf7UPHHDZVOnX1FHrKO','test3','李逵','',1,'2013-10-28',1475754962602,'测试-0','java developer','13892346782','test3@126.com','444444',NULL,1,'测试账户可以删除','2019-08-30 12:04:56','2020-04-26 09:43:16',NULL),(1550649770475,'test4','{bcrypt}$2a$10$dtcSa6LXi23V6On3R5gCNOaVD4nKjLQxYjNKHN5s0oUK1tTZO2AbC','test4','王昭君','',2,'2014-01-27',1550131605508,'测试-1','测试员','13987654355','test4@126.com','555555',NULL,1,'测试用户,可以删除','2019-08-30 12:04:56','2020-04-26 09:43:16',NULL),(556414663386988544,'test5','{bcrypt}$2a$10$2E9u8CLLqYzS25xHe2bG5eStJEIq5X5NRRIFRCXRPygt6oQKj9mk.','test5','贾宝玉','',2,'2019-02-25',1550649343046,'测试-1-3-0','AI开发者','156984','159654@qq.com','169542',NULL,0,'','2019-08-30 12:04:56','2020-04-26 09:42:11',NULL),(556415289206505472,'test6','{bcrypt}$2a$10$9AZhF9FsrB8gMDqwkes6F.5bhFdfxSTRWvybsZRrfZegNyKfgt/6a','test6','刘备','',2,'2010-12-31',1550649243906,'测试-1-3','AI 高级开发','741258','741258@qq.com','741258',NULL,1,'测试人员','2019-08-30 12:04:56','2020-04-26 09:42:11',NULL),(556419366778830848,'test7','{bcrypt}$2a$10$ESW1XqiU78Bdg0PjzUOLi.fDcgI1PtI2fEISc5H5k3f440hoYyDvq','test7','诸葛亮','',2,'2019-03-15',1550134807976,'测试-1-2','测试人员','16354259365','test7@126.com','5963258415',NULL,1,'测试用户','2019-08-30 12:04:56','2020-04-26 09:42:11',NULL),(556420807153156096,'test8','{bcrypt}$2a$10$UrLZb1xfSejF2NO5m.cireoMboEWY7hFpcVc/rcQRlcziVE67pGbW','test8','赵云','',1,'2019-03-16',1475754962602,'测试-0','java 开发程序员','888888','424151385@qq.com','888888',NULL,1,'java 开发人员','2019-08-30 12:04:56','2020-04-26 09:42:11',NULL),(561548712409563136,'test9','{bcrypt}$2a$10$5z6X1pvQzjx0t9WqxG88E.YlqncnYEEOgsIegbiN28NoJyGK/.Ooi','test9','张飞','',2,'2005-03-03',1475754962602,'测试-0','测试程序员','16354259356','16354259356@163.com','16354259356',NULL,1,'测试数据','2019-08-30 12:04:56','2020-04-26 09:42:11',NULL);
/*!40000 ALTER TABLE `pub_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_user_authority`
--

DROP TABLE IF EXISTS `pub_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_user_authority` (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `authority_id` bigint NOT NULL COMMENT '权限ID',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`user_id`,`authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_user_authority`
--

LOCK TABLES `pub_user_authority` WRITE;
/*!40000 ALTER TABLE `pub_user_authority` DISABLE KEYS */;
INSERT INTO `pub_user_authority` (`user_id`, `authority_id`, `update_time`, `create_time`) VALUES (1,1111111111111,'2020-07-12 21:14:15','2020-07-12 21:14:15'),(1,731203661669072896,'2020-07-12 21:14:15','2020-07-12 21:14:15'),(1,731235110652739584,'2020-07-12 21:14:15','2020-07-12 21:14:15'),(1,731235339728846848,'2020-07-12 21:14:15','2020-07-12 21:14:15'),(1,731460575711723520,'2020-07-12 21:14:15','2020-07-12 21:14:15'),(1550215149448,1111111111111,'2020-07-10 11:12:09','2020-07-10 11:12:09');
/*!40000 ALTER TABLE `pub_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_user_role`
--

DROP TABLE IF EXISTS `pub_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_user_role` (
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_user_role`
--

LOCK TABLES `pub_user_role` WRITE;
/*!40000 ALTER TABLE `pub_user_role` DISABLE KEYS */;
INSERT INTO `pub_user_role` (`user_id`, `role_id`, `create_time`, `update_time`) VALUES (1,377351043380,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(1475754971209,400838519752,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(1526264598235,400838519752,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(1550215149448,1550481293329,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(1550469685011,1550649885862,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(1550649666525,1550481293329,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(1550649666525,1550649885862,'2019-08-30 12:05:13','2019-08-30 12:05:13'),(561548712409563136,561551221077311488,'2019-08-30 12:05:13','2019-08-30 12:05:13');
/*!40000 ALTER TABLE `pub_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_version_info`
--

DROP TABLE IF EXISTS `pub_version_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_version_info` (
  `version_id` bigint NOT NULL AUTO_INCREMENT COMMENT '版本id',
  `version_code` int DEFAULT NULL COMMENT '版本号',
  `version_name` varchar(128) DEFAULT NULL COMMENT '版本名称',
  `title` varchar(256) DEFAULT NULL COMMENT '本次版本发布标题',
  `content` text COMMENT '本次版本发布内容',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='版本信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_version_info`
--

LOCK TABLES `pub_version_info` WRITE;
/*!40000 ALTER TABLE `pub_version_info` DISABLE KEYS */;
INSERT INTO `pub_version_info` (`version_id`, `version_code`, `version_name`, `title`, `content`, `create_time`, `publish_time`) VALUES (1,1,'v2.0.5','新增版本信息更改','&nbsp;<br />\r\n<img src=\"http://127.0.0.1:8083/weike-mgt/static/common/ckeditor/plugins/hkemoji/sticker/onion/Onion--43.gif\" style=\"height:50px; width:50px\" />\r\n<ul>\r\n	<li>版本管理</li>\r\n	<li>代码优化</li>\r\n	<li>开发路线</li>\r\n	<li>其他更改</li>\r\n</ul>\r\n&nbsp;','2020-06-21 09:59:32','2020-06-21 09:59:33'),(2,2,'v2.0.6','测试版本','<ul>\r\n	<li>1. [U]可以动态更改演示模式</li>\r\n	<li>2. [F]修复部署环境无法获取jar中的静态资源问题</li>\r\n	<li>3. [O]优化无用的静态文件</li>\r\n	<li>4. [O]js逻辑逐渐向原生迁移</li>\r\n	<li>5. [U]修改前端部分ui显示问题</li>\r\n</ul>\r\n','2020-06-21 11:34:36','2020-06-21 11:34:36'),(3,3,'v2.0.7','测试版本标题','<ul class=\"list-group list-group-flush\">\r\n	<li class=\"list-group-item\">1. [U]可以动态更改演示模式</li>\r\n	<li class=\"list-group-item\">2. [F]修复部署环境无法获取jar中的静态资源问题</li>\r\n	<li class=\"list-group-item\">3. [O]优化无用的静态文件</li>\r\n	<li class=\"list-group-item\">4. [O]js逻辑逐渐向原生迁移</li>\r\n	<li class=\"list-group-item\">5. [U]修改前端部分ui显示问题</li>\r\n</ul>\r\n','2020-06-21 11:42:57','2020-06-21 11:42:57'),(4,0,'v2.0.4','v2.0.4','<ul class=\"list-group list-group-flush\">\r\n	<li class=\"list-group-item\">1. [U]可以动态更改演示模式</li>\r\n	<li class=\"list-group-item\">2. [F]修复部署环境无法获取jar中的静态资源问题</li>\r\n	<li class=\"list-group-item\">3. [O]优化无用的静态文件</li>\r\n	<li class=\"list-group-item\">4. [O]js逻辑逐渐向原生迁移</li>\r\n	<li class=\"list-group-item\">5. [U]修改前端部分ui显示问题</li>\r\n</ul>\r\n','2020-06-22 21:54:24','2020-06-20 00:00:00'),(5,0,'v2.0.3','v2.0.3','<ul>\r\n	<li class=\"list-group-item\">1. [U]代码生成全面升级，可以根据项目自动生成</li>\r\n	<li class=\"list-group-item\">2. [F]修复部分bug</li>\r\n	<li class=\"list-group-item\">3. [O]优化无用的静态文件</li>\r\n	<li class=\"list-group-item\">4. [O]js逻辑逐渐向原生迁移</li>\r\n	<li class=\"list-group-item\">5. [A]新增部分工具类，逐渐替换掉其他三方工具类</li>\r\n	<li class=\"list-group-item\">6. [A]动态修改试图模板</li>\r\n	<li class=\"list-group-item\">7. [A]学习使用spring spi</li>\r\n</ul>\r\n','2020-06-22 21:55:51','2020-06-05 00:00:00'),(6,0,'v2.0.2','v2.0.2','<li class=\"list-group-item\">1. [U]代码生成全面升级，可以根据项目自动生成</li>\r\n                                <li class=\"list-group-item\">2. [F]修复部分bug</li>\r\n                                <li class=\"list-group-item\">3. [O]优化无用的静态文件</li>\r\n                                <li class=\"list-group-item\">4. [O]js逻辑逐渐向原生迁移</li>\r\n                                <li class=\"list-group-item\">5. [A]新增部分工具类，逐渐替换掉其他三方工具类</li>\r\n                                <li class=\"list-group-item\">6. [A]新增文档markdown(未完善)</li>','2020-06-22 21:56:43','2020-05-30 00:00:00'),(7,0,'v2.0.1','v2.0.1','<li class=\"list-group-item\">1. 更改前端UI</li>\r\n                                <li class=\"list-group-item\">2. 修复部分bug</li>\r\n                                <li class=\"list-group-item\">3. 添加项目代码预览</li>\r\n                                <li class=\"list-group-item\">4. 添加代码生成预览</li>\r\n                                <li class=\"list-group-item\">5. 精简代码</li>\r\n                                <li class=\"list-group-item\">6. 添加前端UI demo演示</li>\r\n                                <li class=\"list-group-item\">7. 添加二维码生成</li>\r\n                                <li class=\"list-group-item\">8. 添加文档预览</li>\r\n                                <li class=\"list-group-item\">9. 添加通过拖拽构造form表单</li>','2020-06-22 21:57:27','2020-05-09 00:00:00'),(8,0,'v2.0.0','v2.0.0','<ul class=\"list-group list-group-flush\">\r\n                                <li class=\"list-group-item\">1. 将公共代码移植到common包</li>\r\n                                <li class=\"list-group-item\">2. 改为单页面应用</li>\r\n                                <li class=\"list-group-item\">3. 主键完善功能</li>\r\n                                <li class=\"list-group-item\">4. 完善代码生产</li>\r\n                                <li class=\"list-group-item\">5. 改为hash路，监听hash变化，改变内容</li>\r\n                            </ul>','2020-06-22 21:58:17','2020-03-25 00:00:00'),(9,0,' v1.9.10',' v1.9.10','<ul class=\"list-group list-group-flush\">\r\n                                <li class=\"list-group-item\">1. spring 升级 5.x</li>\r\n                                <li class=\"list-group-item\">2. 统一mybatis mapper文件名</li>\r\n                                <li class=\"list-group-item\">3. 修改前端查看一次机构树，需要两次请求的问题（改为一次）</li>\r\n                                <li class=\"list-group-item\">4. 添加注解日志，记录用户的操作</li>\r\n                                <li class=\"list-group-item\">5. 修改前段部分样式错误</li>\r\n                                <li class=\"list-group-item\">6. 修改数据库中NULL值，改为空字符串</li>\r\n                            </ul>','2020-06-22 21:59:08','2019-04-08 00:00:00');
/*!40000 ALTER TABLE `pub_version_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `pub_dictionary_child_count_view`
--

/*!50001 DROP VIEW IF EXISTS `pub_dictionary_child_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_dictionary_child_count_view` AS select `pub_dictionary`.`pid` AS `dict_id`,count(`pub_dictionary`.`pid`) AS `child_num` from `pub_dictionary` group by `pub_dictionary`.`pid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pub_dictionary_count_view`
--

/*!50001 DROP VIEW IF EXISTS `pub_dictionary_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_dictionary_count_view` AS select `a`.`dictId` AS `dict_id`,`b`.`pid` AS `dict_pid`,`a`.`childNum` AS `child_num` from ((select `pub_dictionary`.`pid` AS `dictId`,count(`pub_dictionary`.`pid`) AS `childNum` from `pub_dictionary` group by `pub_dictionary`.`pid`) `a` left join `pub_dictionary` `b` on((`a`.`dictId` = `b`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pub_menu_child_count_view`
--

/*!50001 DROP VIEW IF EXISTS `pub_menu_child_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_menu_child_count_view` AS select `pub_menu`.`menu_pid` AS `menu_id`,count(`pub_menu`.`menu_pid`) AS `child_num` from `pub_menu` group by `pub_menu`.`menu_pid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pub_menu_count_view`
--

/*!50001 DROP VIEW IF EXISTS `pub_menu_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_menu_count_view` AS select `a`.`menuId` AS `menu_id`,`b`.`menu_pid` AS `menu_pid`,`a`.`childNum` AS `child_num` from ((select `pub_menu`.`menu_pid` AS `menuId`,count(`pub_menu`.`menu_pid`) AS `childNum` from `pub_menu` group by `pub_menu`.`menu_pid`) `a` left join `pub_menu` `b` on((`a`.`menuId` = `b`.`menu_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pub_org_child_count_view`
--

/*!50001 DROP VIEW IF EXISTS `pub_org_child_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_org_child_count_view` AS select `pub_org`.`pid` AS `org_id`,count(`pub_org`.`pid`) AS `child_num` from `pub_org` group by `pub_org`.`pid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pub_org_count_view`
--

/*!50001 DROP VIEW IF EXISTS `pub_org_count_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pub_org_count_view` AS select `a`.`orgId` AS `org_id`,`b`.`pid` AS `org_pid`,`a`.`childNum` AS `child_num` from ((select `pub_org`.`pid` AS `orgId`,count(`pub_org`.`pid`) AS `childNum` from `pub_org` group by `pub_org`.`pid`) `a` left join `pub_org` `b` on((`a`.`orgId` = `b`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-12 16:19:17
