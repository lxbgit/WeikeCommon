-- MySQL dump 10.13  Distrib 8.0.20, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: weike-admin-secondary
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pub_log`
--

DROP TABLE IF EXISTS `pub_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_log` (
  `log_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '全类名',
  `message` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作信息',
  `error` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '错误信息',
  `user_id` bigint DEFAULT NULL COMMENT '操作用户id',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作用户名',
  `ip` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'INFO' COMMENT '日志级别',
  `op_type` int NOT NULL DEFAULT '0' COMMENT '操作类型，参见Log类',
  `op_status` int NOT NULL DEFAULT '1' COMMENT '参见表结构解释；\n操作状态：success/fail',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_log`
--

LOCK TABLES `pub_log` WRITE;
/*!40000 ALTER TABLE `pub_log` DISABLE KEYS */;
INSERT INTO `pub_log` (`log_id`, `name`, `message`, `error`, `user_id`, `username`, `ip`, `level`, `op_type`, `op_status`, `op_time`) VALUES (1,'com.simple.bsp.authority.web.controller.AuthorityController.updateAuthority','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2019-04-01 16:49:29'),(2,'com.simple.bsp.authority.web.controller.AuthorityController.updateAuthority','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2019-04-01 17:55:46'),(3,'com.simple.bsp.authority.web.controller.AuthorityController.addAuthMenu','新增[权限-菜单]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-01 17:59:11'),(4,'com.simple.bsp.authority.web.controller.AuthorityController.updateAuthority','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2019-04-02 10:12:33'),(5,'com.simple.bsp.authority.web.controller.AuthorityController.saveAuthority','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-02 10:31:16'),(6,'com.simple.bsp.authority.web.controller.AuthorityController.addAuthMenu','新增[权限-菜单]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-02 10:32:01'),(7,'com.simple.bsp.authority.web.controller.AuthorityController.addAuthMenu','新增[权限-菜单]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-13 11:20:01'),(8,'com.simple.bsp.authority.web.controller.AuthorityController.addAuthMenu','新增[权限-菜单]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-13 11:36:05'),(9,'com.simple.bsp.authority.web.controller.AuthorityController.addAuthMenu','新增[权限-菜单]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-13 17:07:41'),(10,'com.simple.bsp.authority.web.controller.AuthorityController.addAuthMenu','新增[权限-菜单]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2019-04-13 17:14:51'),(11,'com.lxb.weike.bsp.web.controller.AuthorityController.addAuthResource','新增[权限-资源]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-06-20 22:24:54'),(12,'com.lxb.weike.bsp.web.controller.AuthorityController.deleteAllById','删除权限',NULL,1,'admin','127.0.0.1','INFO',4,1,'2020-06-20 22:26:10'),(13,'com.lxb.weike.bsp.web.controller.AuthorityController.addAuthResource','新增[权限-资源]对应关系',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-06-23 17:25:42'),(14,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-07 22:12:16'),(15,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-10 17:42:18'),(16,'com.lxb.weike.bsp.web.controller.AuthorityController.updateById','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2020-07-10 17:43:14'),(17,'com.lxb.weike.bsp.web.controller.AuthorityController.updateById','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2020-07-10 17:43:35'),(18,'com.lxb.weike.bsp.web.controller.AuthorityController.updateById','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2020-07-10 17:44:01'),(19,'com.lxb.weike.bsp.web.controller.AuthorityController.updateById','更新权限',NULL,1,'admin','127.0.0.1','INFO',3,1,'2020-07-10 17:44:20'),(20,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-10 19:47:16'),(21,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-10 19:48:11'),(22,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-11 10:43:11'),(23,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-11 11:07:55'),(24,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-11 11:08:45'),(25,'com.lxb.weike.bsp.web.controller.AuthorityController.create','添加权限',NULL,1,'admin','127.0.0.1','INFO',2,1,'2020-07-11 11:36:58');
/*!40000 ALTER TABLE `pub_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_mail`
--

DROP TABLE IF EXISTS `pub_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_mail` (
  `id` bigint NOT NULL,
  `user_id` bigint NOT NULL COMMENT '发送者用户',
  `subject` varchar(256) DEFAULT NULL COMMENT '邮件标题',
  `content` text COMMENT '发送内容',
  `create_time` datetime DEFAULT NULL COMMENT '发送时间',
  `status` tinyint DEFAULT NULL COMMENT '邮件状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='邮件管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_mail`
--

LOCK TABLES `pub_mail` WRITE;
/*!40000 ALTER TABLE `pub_mail` DISABLE KEYS */;
INSERT INTO `pub_mail` (`id`, `user_id`, `subject`, `content`, `create_time`, `status`) VALUES (549954781909614592,1,'邮件主题1','版权声明：本文内容由互联网用户自发贡献，版权归作者所有，本社区不拥有所有权，也不承担相关法律责任。如果您发现本社区中有涉嫌抄袭的内容，欢迎发送邮件至：yqgroup@service.aliyun.com 进行举报，并提供相关证据，一经查实，本社区将立刻删除涉嫌侵权内容。','2019-02-26 14:03:57',1),(549954782190632960,1,'邮件主题2','摘要： 版权声明：本文为 testcs_dn(微wx笑) 原创文章，非商用自由转载-保持署名-注明出处，谢谢。	https://blog.csdn.net/testcs_dn/article/details/82016025	Illegal mix of collations (utf8_general_ci,IMPLICIT) and (utf8mb4_general_ci,COERCIBLE) for operation \'like\' 这个问题一看就是编码的问题。','2019-02-26 14:03:57',1),(549964160205586432,1,'邮件主题3','呵呵呵呵<s>呵呵哈哈哈</s><em>啊哈哈哈哈航啊</em>','2019-02-26 14:41:13',1),(549964160893452288,1,'邮件主题4','呵呵呵呵<s>呵呵哈哈哈</s><em>啊哈哈哈哈航啊</em>','2019-02-26 14:41:13',1),(549964160893452289,1,'邮件主题5','呵呵呵呵<s>呵呵哈哈哈</s><em>啊哈哈哈哈航啊</em>','2019-02-26 14:41:13',1),(572016743896055808,0,'测试发送邮件给test2和test7','<pre>\r\n/**\r\n * 获取该mailId的所有接受者\r\n *\r\n * @param mailId {@link Mail#getId()}\r\n */\r\n@Override\r\npublic List&lt;User&gt; findAllReceivers(String mailId) {\r\n    return mailReceiverMapper.findAllReceivers(mailId);\r\n}\r\n\r\n\r\n</pre>\r\n\r\n<p>而使用dataset属性,我们根本不需要任何循环去获取你想要的那个值,直接一行秒杀:</p>\r\n\r\n<pre>\r\n<code>expense=document.getElementById(&#39;day-meal-expense&#39;).dataset;</code></pre>\r\n\r\n<ul>\r\n	<li>1</li>\r\n</ul>\r\n','2019-04-28 11:10:19',1),(572098080669696000,0,'测试发送邮件','<p>我原来的解决办法是,对原字符串逐个字符获取其长度,如果超过3个字节,那么则编码下,就能存储了,依照上次的经验,我写了如下代码</p>\r\n\r\n<pre>\r\n\r\n&nbsp;</pre>\r\n\r\n<ol>\r\n	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;String&nbsp;a&nbsp;=&nbsp;&quot;\\uD83D\\uDE01&quot;;//&nbsp;一个&nbsp;emoji&nbsp;表情</li>\r\n	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;byte[]&nbsp;bytes&nbsp;=&nbsp;a.getBytes(&quot;utf-8&quot;);</li>\r\n	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(bytes.length);//&nbsp;输出&nbsp;4</li>\r\n</ol>\r\n','2019-04-28 16:33:32',1);
/*!40000 ALTER TABLE `pub_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pub_mail_receiver`
--

DROP TABLE IF EXISTS `pub_mail_receiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pub_mail_receiver` (
  `id` bigint NOT NULL,
  `mail_id` bigint NOT NULL COMMENT '对应的邮件信息',
  `sender_id` bigint NOT NULL,
  `sender_email` varchar(32) DEFAULT NULL COMMENT '发送者邮箱',
  `receiver_id` bigint DEFAULT NULL,
  `receiver_email` varchar(32) DEFAULT NULL COMMENT '接收者邮箱',
  `status` tinyint DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='邮件接收信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pub_mail_receiver`
--

LOCK TABLES `pub_mail_receiver` WRITE;
/*!40000 ALTER TABLE `pub_mail_receiver` DISABLE KEYS */;
INSERT INTO `pub_mail_receiver` (`id`, `mail_id`, `sender_id`, `sender_email`, `receiver_id`, `receiver_email`, `status`, `send_time`) VALUES (572016743904444416,572016743896055808,0,NULL,1550469685011,NULL,1,'2019-04-28 11:10:19'),(572016743904444417,572016743896055808,0,NULL,556419366778830848,NULL,1,'2019-04-28 11:10:19'),(572098080673890304,572098080669696000,0,NULL,556420807153156096,NULL,1,'2019-04-28 16:33:32');
/*!40000 ALTER TABLE `pub_mail_receiver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_article`
--

DROP TABLE IF EXISTS `t_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_article` (
  `article_id` bigint NOT NULL AUTO_INCREMENT,
  `author_id` bigint DEFAULT NULL,
  `title` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `subtitle` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '子标题',
  `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `comment_count` bigint DEFAULT '0',
  `edit_mode` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_article`
--

LOCK TABLES `t_article` WRITE;
/*!40000 ALTER TABLE `t_article` DISABLE KEYS */;
INSERT INTO `t_article` (`article_id`, `author_id`, `title`, `subtitle`, `content`, `image`, `comment_count`, `edit_mode`, `create_time`, `update_time`) VALUES (1,1,'让网络空间命运共同体更具生机活力',NULL,'——习近平主席致第五届世界互联网大会贺信引发中外人士共鸣\r\n\r\n察看托老所 关注老有所养\r\n\r\n11月6日上午，习近平来到虹口区市民驿站嘉兴路街道第一分站，与居民和社区工作者亲切交谈，并祝他们生活愉快。\r\n\r\n嘉兴路街道第一分站，是虹口区设置的35个市民驿站之一。为满足居民社区生活各方面需求，虹口区努力打造“15分钟社区生活服务圈”。\r\n\r\n在托老所老年人日间照护中心，几位年龄在79至92岁的老年居民正在制作手工艺品，总书记亲切向他们问好，并与他们一一握了手。老人们激动地握着总书记的手，向总书记描述自己的幸福晚年生活。有位老爷爷和总书记握了三次手。\r\n\r\n习近平指出，我国已经进入老龄社会，让老年人老有所养、生活幸福、健康长寿是我们的共同愿望。党中央高度重视养老服务工作，要把政策落实到位，惠及更多老年人。',NULL,0,'MARKDOWN','2018-11-08 04:13:10','2018-11-08 04:13:10'),(2,1,'测试add article',NULL,'//        Authentication object = SecurityContextHolder.getContext().getAuthentication();\r\n```java\r\n private final PostService postService;\r\n    private final ReplyService replyService;\r\n\r\n    @Autowired\r\n    public PostController(PostService postService, ReplyService replyService) {\r\n        this.postService = postService;\r\n        this.replyService = replyService;\r\n    }\r\n```',NULL,NULL,'MARKDOWN','2019-05-24 03:01:22','2019-05-24 03:01:22'),(3,1,'测试添加文章标题1','测试添加文章副标题1','```\r\n<insert id=\"insertAndGetId\" useGeneratedKeys=\"true\" keyProperty=\"userId\" parameterType=\"com.chenzhou.mybatis.User\">\r\n    insert into user(userName,password,comment)\r\n    values(#{userName},#{password},#{comment})\r\n</insert>\r\n```\r\n\r\n> `useGeneratedKeys=\"true\"` 表示给主键设置自增长\r\n`keyProperty=\"userId\"`  表示将自增长后的Id赋值给实体类中的`userId`字段。\r\n`parameterType=\"com.chenzhou.mybatis.User\"` 这个属性指向传递的参数实体类\r\n\r\n> 这里提醒下，`<insert></insert>` 中没有`resultType`属性，不要乱加。\r\n\r\n> 实体类中`uerId` 要有`getter() and setter();` 方法',NULL,0,'MARKDOWN','2019-05-29 15:53:27','2019-05-29 15:53:27'),(4,1,'测试添加文章标题2','测试添加文章副标题2','# 2.0\r\n\r\n```\r\n<insert id=\"insertAndGetId\" useGeneratedKeys=\"true\" keyProperty=\"userId\" parameterType=\"com.chenzhou.mybatis.User\">\r\n    insert into user(userName,password,comment)\r\n    values(#{userName},#{password},#{comment})\r\n</insert>\r\n```\r\n\r\n> `useGeneratedKeys=\"true\"` 表示给主键设置自增长\r\n`keyProperty=\"userId\"`  表示将自增长后的Id赋值给实体类中的`userId`字段。\r\n`parameterType=\"com.chenzhou.mybatis.User\"` 这个属性指向传递的参数实体类\r\n\r\n> 这里提醒下，`<insert></insert>` 中没有`resultType`属性，不要乱加。\r\n\r\n> 实体类中`uerId` 要有`getter() and setter();` 方法',NULL,0,'MARKDOWN','2019-05-29 15:54:44','2019-05-29 15:54:44'),(5,1,'MyBatis获取 insert 返回的主键','MyBatis获取 insert 返回的主键','# MyBatis获取 insert 返回的主键\r\n\r\n## 方法一：使用JDBC方式返回主键自增的值\r\n\r\nuseGeneratedKeys设置为true后，Mybatis会使用JDBC的 getGeneratedKeys方法来取出由数据库内部生成的主键。获得主键值后将其赋值给 keyProperty配置的id属性。当需要设置多个属性时，使用逗号隔开，这种情况下还需要设置 keyColumn属性，按照顺序指定数据库的列，这里列的值会和keyProperty配置的属性一一对应。\r\n\r\n\r\n## 方法二：使用selectKey返回主键的值\r\n\r\nMySql 使用 SELECT LAST_INSERT_ID() 。',NULL,0,'MARKDOWN','2019-05-29 16:00:11','2019-05-29 16:00:11'),(6,1,'测试添加文章标题3','测试添加文章副标题3','# 发文须知\r\n\r\nEtiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.',NULL,0,'MARKDOWN','2019-05-29 16:04:32','2019-05-29 16:04:32');
/*!40000 ALTER TABLE `t_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_comment`
--

DROP TABLE IF EXISTS `t_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_comment` (
  `comment_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `article_id` bigint DEFAULT NULL,
  `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_comment`
--

LOCK TABLES `t_comment` WRITE;
/*!40000 ALTER TABLE `t_comment` DISABLE KEYS */;
INSERT INTO `t_comment` (`comment_id`, `user_id`, `article_id`, `content`, `create_time`, `update_time`) VALUES (1,1,1,'<p><img src=\"https://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/smiley.png\" class=\"emoji\" title=\":smiley:\" alt=\":smiley:\"> <img src=\"https://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/anguished.png\" class=\"emoji\" title=\":anguished:\" alt=\":anguished:\"> <img src=\"https://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/open_mouth.png\" class=\"emoji\" title=\":open_mouth:\" alt=\":open_mouth:\"> <img src=\"https://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/grimacing.png\" class=\"emoji\" title=\":grimacing:\" alt=\":grimacing:\"> <img src=\"https://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/neckbeard.png\" class=\"emoji\" title=\":neckbeard:\" alt=\":neckbeard:\"> <img src=\"https://www.webpagefx.com/tools/emoji-cheat-sheet/graphics/emojis/green_heart.png\" class=\"emoji\" title=\":green_heart:\" alt=\":green_heart:\"></p>\n<p>测试评论</p>\n','2018-11-08 04:13:56','2018-11-08 04:13:56');
/*!40000 ALTER TABLE `t_comment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-12 16:19:39
