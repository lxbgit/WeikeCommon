# 更新日志

## 最新

### Update at 2020-05-30
- 模板独立出来，参见【module-templates】模块
- 前端模块化，可以动态修改试图

## 以前
### Update at 2020-05-09
- 更改前端UI
- 修复部分bug
- 添加项目代码预览
- 添加代码生成预览
- 精简代码
- 添加前端UI demo演示
- 添加二维码生成
- 添加文档预览
- 添加通过拖拽构造form表单

### Update at 2020-03-25
- 将公共代码移植到common包

### Update at 2020-03-01
- 改为单页面应用
- 主键完善功能
- 完善代码生产
- 改为hash路，监听hash变化，改变内容


### v5.0.3 (2019-05-09)
> Update at 2019-05-09
- 修复已知bug
- 修改数据库ID类型
- 权限变化事件监听

### v5.0.2 (2019-04-25)
> Update at 2019-04-25
- 代码生成模板
- 修复已知bug
- 常规维护

### v5.0.1 (2019-04-19)
> Update at 2019-04-19
- 重构 SpringSecurity
- 常规维护

### v5.0.0
> Update at 2019-04-03
- 在v4.2的基础上更新
- 升级Spring 5.x
- 常规维护

### v4.2.0

> Update at 2019-04-02
- 添加注解日志，记录用户的操作
- 修改前段部分样式错误
- 修改数据库中NULL值，改为空字符串

> Update at 2019-03-29

- jsp -> FreeMaker
- Bootstrap v4.3
- 重构 `[用户]-[角色]-[权限]-[资源]` 结构
- 添加注释
- 修改前段部分页面逻辑
- 版本号从4.0开始

### v2.0.1
add:
- Bootstrap v4.3
- Mybatis


### v2.0.0

后端使用的技术：

- SpringMVC
- Spring Security

- MySQL 8.x
- Quartz
- Swagger

前端：

- Jquery
- EesyUI v1.7
- FreeMaker(模板)
- ckeditor
- ztree